package com.securemetric.jce;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyStore;
import java.security.Provider;
import java.security.Security;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.throwable.SMVNException;

public class PKCS11Lib {

    private final Logger LOGGER = LogManager.getLogger(PKCS11Lib.class);
    private Provider provider;
    private String providerName;

    public PKCS11Lib(String libPath, int slotId)
        throws SMVNException {
        LOGGER.debug("Init provider");
        this.providerName = "SunPKCS11-" + UUID.randomUUID().toString();
        StringBuilder providerConf = generateProviderConf(libPath, slotId);
        initProvider(providerConf.toString());

    }

    private StringBuilder generateProviderConf(String libPath, int slotId) {
        StringBuilder providerConf = new StringBuilder();
        providerConf.append("name=").append(this.providerName).append("\n");
        providerConf.append("library=").append(libPath).append("\n");
        providerConf.append("slot=").append(slotId);
        return providerConf;
    }

    private void initProvider(String providerConf) throws SMVNException {
        // get provider from repository
        this.provider = Security.getProvider(this.providerName);
        if (provider == null) {
            LOGGER.debug(String.format("provider %s is null, register it", this.providerName));
            // register the Providers
            InputStream is = null;
            try {
                is = new ByteArrayInputStream(providerConf.getBytes());
                this.provider = new sun.security.pkcs11.SunPKCS11(is);
            } catch (Exception ex) {
                throw new SMVNException(ex);
            } finally {
                if (is != null) {
                    try {
                        is.close();
                        LOGGER.debug("Input stream closed successfully");
                    } catch (IOException e) {
                        LOGGER.error("Input stream closed failed ", e);
                    }
                }
            }

            Security.addProvider(this.provider);
        }

    }

    public KeyStore getKeyStore(String slotPIN) throws SMVNException {
        KeyStore keyStore;
        try {
            LOGGER.debug("Loading key store");
            long start = System.currentTimeMillis();
            keyStore = KeyStore.getInstance("PKCS11", this.provider);
            keyStore.load(null, slotPIN.toCharArray());
            LOGGER.debug("Loading key store takes " + (System.currentTimeMillis() - start));
            return keyStore;
        } catch (Exception e) {
            LOGGER.error("getKeyStore error ", e);
            throw new SMVNException(e);
        }
    }

    public void destroyProvider() {
        Security.removeProvider(this.providerName);
    }

    public String getProviderName() {
        return providerName;
    }

    public void setProviderName(String providerName) {
        this.providerName = providerName;
    }

}

