package com.securemetric.jce;

import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Enumeration;

public class PKCS11KeyWrapper {

    private PrivateKey privateKey;
    private Certificate[] certChain;
    
    public PKCS11KeyWrapper(PrivateKey privateKey, Certificate[] certChain) {
        this.privateKey = privateKey;
        this.certChain = certChain;
    }

    public PrivateKey getPrivateKey() {
        return privateKey;
    }
    
    public void setPrivateKey(PrivateKey privateKey) {
        this.privateKey = privateKey;
    }
    
    public Certificate[] getCertChain() {
        return certChain;
    }
    
    public void setCertChain(Certificate[] certChain) {
        this.certChain = certChain;
    }

    public static PKCS11KeyWrapper getKeyStore(String keyStoreFile,
			String keyStorePwd) throws Exception {
		PKCS11KeyWrapper keyStore = null;
		KeyStore ks = KeyStore.getInstance("pkcs12");
		ks.load(PKCS11KeyWrapper.class.getResourceAsStream(keyStoreFile), keyStorePwd.toCharArray());
		Enumeration<String> aliasesEnum = ks.aliases();
		PrivateKey privateKey = null;
		Certificate[] certificateChain = null;
		if (aliasesEnum.hasMoreElements()) {
			String alias = (String) aliasesEnum.nextElement();
			certificateChain = ks.getCertificateChain(alias);
			privateKey = (PrivateKey) ks.getKey(alias,
					keyStorePwd.toCharArray());
			keyStore = new PKCS11KeyWrapper(privateKey, certificateChain);
		}
		return keyStore;
	}

}
