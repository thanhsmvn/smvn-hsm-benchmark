package com.securemetric.jce;

import java.io.FileInputStream;
import java.security.Key;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.throwable.SMVNException;

public class PKCS11KeyStoreBean {

    private static final Logger LOGGER = LogManager.getLogger(PKCS11KeyStoreBean.class);
    private final KeyStore keyStore;
    private final PKCS11Lib pkcs11Lib;

    private static PKCS11KeyStoreBean instance;

    private PKCS11KeyStoreBean()
        throws SMVNException {
        Properties properties = new Properties();
        try {
            properties.load(new FileInputStream("hsm_config.properties"));
            String libraryPath = properties.getProperty("library_path");
            LOGGER.debug("getKeyStoreBean - library_path = " + libraryPath);
            String tokenPIN = properties.getProperty("token_pin", "0");
            String slotIdStr = properties.getProperty("slot_id", "0");
            LOGGER.debug("getKeyStoreBean - slot_id = " + slotIdStr);
            int slotId = Integer.parseInt(slotIdStr);
            this.pkcs11Lib = new PKCS11Lib(libraryPath, slotId);
            this.keyStore = this.pkcs11Lib.getKeyStore(tokenPIN);
        } catch (Exception e) {
            LOGGER.error("Cannot construct PKCS11KeyStoreBean", e);
            throw new SMVNException(e);
        }

    }

    public static PKCS11KeyStoreBean getInstance() throws SMVNException {
        if (instance == null) {
            LOGGER.debug("initiallize IHTKKKeyStoreBean instance...");
            instance = new PKCS11KeyStoreBean();
        }
        return instance;
    }

    public PrivateKey getPrivateKey(String keyAlias) throws SMVNException {
        try {
            LOGGER.debug("getPrivateKey start");
            long start = System.currentTimeMillis();
            if (!this.keyStore.containsAlias(keyAlias)) {
                LOGGER.debug("getPrivateKey error");
                throw new SMVNException(String.format("Key with alias %s not found!", keyAlias));
            }
            Key key = this.keyStore.getKey(keyAlias, null);
            if (!(key instanceof PrivateKey)) {
                LOGGER.debug("getPrivateKey error");
                throw new SMVNException(String.format("Key with alias %s is not a private key!", keyAlias));
            }
            LOGGER.debug("getPrivateKey takes " + (System.currentTimeMillis() - start));
            return (PrivateKey) key;
        } catch (Exception e) {
            LOGGER.error("Get Private key error", e);
            throw new SMVNException(e);
        }
    }

    public Certificate[] getCertChain(String keyAlias) throws SMVNException {
        try {
            LOGGER.debug("getCertChain start");
            long start = System.currentTimeMillis();

            Certificate[] certificateChain = this.keyStore.getCertificateChain(keyAlias);
            if (certificateChain == null || certificateChain.length == 0) {
                LOGGER.debug("getCertChain error");
                throw new SMVNException(String.format("Certificate with alias %s not found!", keyAlias));
            }
            LOGGER.debug("getCertChain takes " + (System.currentTimeMillis() - start));
            return certificateChain;
        } catch (Exception e) {
            LOGGER.error("getCertChain key error", e);
            throw new SMVNException(e);
        }
    }

    public String getProviderName() {
        if (this.pkcs11Lib == null) {
            LOGGER.debug("fn:getProviderName: pkcs11Lib is not initiallized");
            return null;
        }
        return this.pkcs11Lib.getProviderName();
    }
}
