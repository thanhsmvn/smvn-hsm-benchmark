package com.securemetric.jce;

import java.security.PrivateKey;
import java.security.cert.Certificate;

import com.securemetric.throwable.SMVNException;

public class JCESpecification {

	private final PrivateKey privateKey;
	private final Certificate[] certChain;
	private final byte[] data;
	private final String providerName;

	public JCESpecification(byte[] data)
			throws SMVNException {
		PKCS11KeyStoreBean keyStore = PKCS11KeyStoreBean.getInstance();
		this.privateKey = keyStore.getPrivateKey("vcb_certificate");
		this.certChain = keyStore.getCertChain("vcb_certificate");
		this.data = data;
		this.providerName = keyStore.getProviderName();
	}

	public byte[] getData() {
		return data;
	}

	public PrivateKey getPrivateKey() {
		return privateKey;
	}

	public Certificate[] getCertChain() {
		return certChain;
	}

	public String getProviderName() {
		return providerName;
	}
	
}
