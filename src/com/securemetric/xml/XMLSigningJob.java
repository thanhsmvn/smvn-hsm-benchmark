package com.securemetric.xml;

import java.security.PrivateKey;
import java.security.cert.Certificate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.benchmark.CommonSigningJob;

public class XMLSigningJob extends CommonSigningJob {
	private static final Logger LOGGER = LogManager.getLogger(XMLSigningJob.class);
	private final XMLSpecification spec;

	public XMLSigningJob(int numberOfInterval, XMLSpecification spec) {
		super(numberOfInterval);
		this.spec = spec;
	}

	@Override
	public void process() {
		XMLSigner signer;
		byte[] data = spec.getData();
		PrivateKey privateKey = spec.getPrivateKey();
		Certificate[] certChain = spec.getCertChain();
		int numberOfInterval = this.getNumberOfInterval();
		try {
			signer = new XMLSigner();
			for (int i = 0; i < numberOfInterval; i++) {
				signer.signXMLFileXPathExclusiveSHA1(data, privateKey, certChain, "//Content");
			}

		} catch (Exception e) {
			LOGGER.error(e);
		}
	}

}
