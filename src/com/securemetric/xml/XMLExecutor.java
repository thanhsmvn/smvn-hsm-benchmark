package com.securemetric.xml;

import java.util.concurrent.CountDownLatch;

import com.securemetric.benchmark.BenchmarkExecutor;
import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.throwable.SMVNException;

public class XMLExecutor extends BenchmarkExecutor {
	private final XMLSpecification spec;

	public XMLExecutor(int numberOfThread, int numberOfInterval, byte data[]) throws SMVNException {
		super(numberOfThread, numberOfInterval);
		this.spec = new XMLSpecification(data);
	}

	@Override
	public CommonSigningJob getSinginJob(int currentThread) {
		return new XMLSigningJob(this.getNumberOfInterval(), spec);
	}

}
