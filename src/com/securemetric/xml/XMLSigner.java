package com.securemetric.xml;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import javax.xml.crypto.Data;
import javax.xml.crypto.URIDereferencer;
import javax.xml.crypto.URIReference;
import javax.xml.crypto.URIReferenceException;
import javax.xml.crypto.XMLCryptoContext;
import javax.xml.crypto.XMLStructure;
import javax.xml.crypto.dom.DOMStructure;
import javax.xml.crypto.dsig.CanonicalizationMethod;
import javax.xml.crypto.dsig.DigestMethod;
import javax.xml.crypto.dsig.Reference;
import javax.xml.crypto.dsig.SignatureMethod;
import javax.xml.crypto.dsig.SignatureProperties;
import javax.xml.crypto.dsig.SignatureProperty;
import javax.xml.crypto.dsig.SignedInfo;
import javax.xml.crypto.dsig.Transform;
import javax.xml.crypto.dsig.XMLObject;
import javax.xml.crypto.dsig.XMLSignature;
import javax.xml.crypto.dsig.XMLSignatureFactory;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.KeyValue;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.ExcC14NParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.crypto.dsig.spec.XPathFilter2ParameterSpec;
import javax.xml.crypto.dsig.spec.XPathFilterParameterSpec;
import javax.xml.crypto.dsig.spec.XPathType;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XMLSigner {
	
	private String providerName;
	private XMLSignatureFactory sigFactory;
	
	
	public XMLSigner() throws Exception {
		providerName = System.getProperty("jsr106Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");
		sigFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());
	}
	
	//	Adding new method for SHA1 and SHA-RSA1 security method
	public byte[] signXMLFileXPathExclusiveSHA1(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath) throws Exception {
		SignatureMethod sm = sigFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null);
		DigestMethod dm = sigFactory.newDigestMethod(DigestMethod.SHA1, null); 
		return signXMLFileXPathExclusive(fileData, privateKey, certificateChain, tagPath, sm, dm);
	}
	
	//	Adding new method for SHA256 and SHA-RSA256 security method
	public byte[] signXMLFileXPathExclusiveSHA256(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath) throws Exception {
		DigestMethod dm = sigFactory.newDigestMethod(DigestMethod.SHA256, null); 
		SignatureMethod sm = sigFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null);
		return signXMLFileXPathExclusive(fileData, privateKey, certificateChain, tagPath, sm, dm);
	}
	
	//	Adding more two parameters for SignatureMethod and DigestMethod
	//	Extending SHA and RSA security method configurations	
	public byte[] signXMLFileXPathExclusive(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath, SignatureMethod sm, DigestMethod dm) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		Document doc = dbFactory.newDocumentBuilder().parse(
				new ByteArrayInputStream(fileData));

		String referenceURI = null;
		XPathExpression expr = null;
		NodeList nodes;
		// List transforms = null;
		List<Transform> listTransform = null;
		TransformParameterSpec param = null;
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		expr = xpath.compile(tagPath);
		nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		if (nodes.getLength() < 1) {
			System.err.println("Không tìm thấy node qua PATH: " + tagPath);
			throw new Exception("Không tìm thấy node qua PATH: " + tagPath);
		}
		final String path = "//ancestor-or-self::"
				+ tagPath.substring(tagPath.lastIndexOf("/") + 1);
		referenceURI = "";
		Transform transform = null;
		listTransform = Collections
				.synchronizedList(new ArrayList<Transform>());
		transform = sigFactory.newTransform(Transform.XPATH,
				new XPathFilterParameterSpec(path));
		listTransform.add(transform);

		List<XPathType> xpaths = new ArrayList<XPathType>();
		xpaths.add(new XPathType(path, XPathType.Filter.INTERSECT));
		listTransform = Collections
				.synchronizedList(new ArrayList<Transform>());
		transform = sigFactory.newTransform(Transform.XPATH2,
				new XPathFilter2ParameterSpec(xpaths));
		listTransform.add(transform);

		List<String> prefix = Collections
				.synchronizedList(new ArrayList<String>());
		prefix.add(ExcC14NParameterSpec.DEFAULT);
		param = new ExcC14NParameterSpec(prefix);
		transform = sigFactory.newTransform(
				CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS, param);
		listTransform.add(transform);
		X509Certificate cert = (X509Certificate) certificateChain[0];
		PublicKey publicKey = cert.getPublicKey();

		
//		Reference ref = sigFactory.newReference(referenceURI, sigFactory.newDigestMethod(DigestMethod.SHA1, null), listTransform, null, null);
		Reference ref = sigFactory.newReference(referenceURI, dm, listTransform, null, null);
		
//		Reference tsRef = sigFactory.newReference("#TimeSignature", sigFactory.newDigestMethod(DigestMethod. SHA1, null));
		Reference tsRef = sigFactory.newReference("#TimeSignatureNHTM", dm);
		List<Reference> listRef = Collections.synchronizedList(new ArrayList<Reference>());
		listRef.add(ref);
		listRef.add(tsRef);

		SignedInfo signedInfo = sigFactory.newSignedInfo(sigFactory
				.newCanonicalizationMethod(
						CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS,
						(C14NMethodParameterSpec) param), 
//				sigFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
				sm,
				listRef);

		KeyInfoFactory keyInfoFactory = sigFactory.getKeyInfoFactory();
		List<Object> x509Content = new ArrayList<Object>();
		x509Content.add(cert.getSubjectX500Principal().getName() + "|TGKY="
				+ convertDateToString(new Date(), "DDMMYYYYHH24MISS"));
		x509Content.add(cert);
		X509Data x509Data = keyInfoFactory.newX509Data(x509Content);
		KeyValue keyValue = keyInfoFactory.newKeyValue(publicKey);
		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Arrays.asList(keyValue,
				x509Data));

		DOMSignContext dsc = new DOMSignContext(privateKey,
				doc.getDocumentElement());
		dsc.setURIDereferencer(new URIDereferencer() {
			@Override
			public Data dereference(URIReference uriReference,
					XMLCryptoContext context) throws URIReferenceException {
				final String providerName = System.getProperty(
						"jsr105Provider",
						"org.jcp.xml.dsig.internal.dom.XMLDSigRI");

				XMLSignatureFactory fac = null;
				try {
					fac = XMLSignatureFactory.getInstance("DOM",
							(Provider) Class.forName(providerName)
									.newInstance());
				} catch (Exception e) {
				}
				

				Data data = fac.getURIDereferencer().dereference(uriReference,
						context);
				return data;
			}
		});

		Element dateTimeStampElement = doc.createElement("DateTimeStamp");
		dateTimeStampElement.setTextContent(convertDateToString(new Date(), "DDMMYYYYHH24MISS"));
		//Node node = doc.createTextNode();
		XMLStructure content = new DOMStructure(dateTimeStampElement);
		SignatureProperty sp = sigFactory.newSignatureProperty(Collections.singletonList(content),
				"signatureProperties", "TimeSignatureNHTM");
		SignatureProperties listSp = sigFactory.newSignatureProperties(Collections.singletonList(sp), null);
		XMLObject obj = sigFactory.newXMLObject(Collections.singletonList(listSp),
				null, null, null);
		
		XMLSignature signature = sigFactory
				.newXMLSignature(signedInfo, keyInfo, Collections.singletonList(obj), null, null);
		 for (int i = 0; i < 5000; i++) {
		signature.sign(dsc);
		 }

		// System.setProperty("javax.xml.transform.TransformerFactory",
		// "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Transformer trans = TransformerFactory.newInstance().newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(os));
		os.flush();
		return os.toByteArray();
	}

	
	//	14/09/17 Cuongdt
	//	Adding new method for SHA1 and SHA-RSA1 security method
	public byte[] signXMLFileXPathEnvelopedSHA1(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath) throws Exception {
		SignatureMethod sm = sigFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null);
		DigestMethod dm = sigFactory.newDigestMethod(DigestMethod.SHA1, null); 
		return signXMLFileXPathEnveloped(fileData, privateKey, certificateChain, tagPath, sm, dm);
	}
	
	//	14/09/17 Cuongdt
	//	Adding new method for SHA256 and SHA-RSA256 security method
	public byte[] signXMLFileXPathEnvelopedSHA256(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath) throws Exception {
		DigestMethod dm = sigFactory.newDigestMethod(DigestMethod.SHA256, null); 
		SignatureMethod sm = sigFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null);
		return signXMLFileXPathEnveloped(fileData, privateKey, certificateChain, tagPath, sm, dm);
	}
	
	//	14/09/17 Cuongdt
	//	Adding more two parameters for SignatureMethod and DigestMethod
	//	Extending SHA and RSA security method configurations
	public byte[] signXMLFileXPathEnveloped(byte[] fileData, PrivateKey privateKey, Certificate[] certificateChain, 
			String tagPath, SignatureMethod sm, DigestMethod dm) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		Document doc = dbFactory.newDocumentBuilder().parse(
				new ByteArrayInputStream(fileData));

//		String providerName = System.getProperty("jsr106Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");
//		final XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());
		
		String referenceURI = null;
		XPathExpression expr = null;
		NodeList nodes;
		// List transforms = null;
		List<Transform> listTransform = null;
		TransformParameterSpec param = null;
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();

		expr = xpath.compile(tagPath);
		nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		if (nodes.getLength() < 1) {
			System.err.println("Không tìm thấy node qua PATH: " + tagPath);
			throw new Exception("Không tìm thấy node qua PATH: " + tagPath);
		}
		final String path = "//"
				+ tagPath.substring(tagPath.lastIndexOf("/") + 1);
		referenceURI = "";
		Transform transform = null;
		listTransform = Collections
				.synchronizedList(new ArrayList<Transform>());
		transform = sigFactory.newTransform(Transform.XPATH,
				new XPathFilterParameterSpec(path));
		listTransform.add(transform);

		List<XPathType> xpaths = new ArrayList<XPathType>();
		xpaths.add(new XPathType(path, XPathType.Filter.INTERSECT));
		listTransform = Collections
				.synchronizedList(new ArrayList<Transform>());
		transform = sigFactory.newTransform(Transform.XPATH2,
				new XPathFilter2ParameterSpec(xpaths));
		listTransform.add(transform);

		List<String> prefix = Collections
				.synchronizedList(new ArrayList<String>());
		prefix.add(ExcC14NParameterSpec.DEFAULT);
		param = new ExcC14NParameterSpec(prefix);
		transform = sigFactory.newTransform(CanonicalizationMethod.ENVELOPED,
				(TransformParameterSpec) null);
		listTransform.add(transform);
		X509Certificate cert = (X509Certificate) certificateChain[0];
		PublicKey publicKey = cert.getPublicKey();

		Reference ref = sigFactory.newReference(referenceURI, dm, listTransform, null, null);
		Reference tsRef = sigFactory.newReference("#TimeSignatureNHTM", dm);
		List<Reference> listRef = Collections.synchronizedList(new ArrayList<Reference>());
		listRef.add(ref);
		listRef.add(tsRef);

		SignedInfo signedInfo = sigFactory.newSignedInfo(sigFactory
				.newCanonicalizationMethod(
						CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS,
						(C14NMethodParameterSpec) param), sm, listRef);

		KeyInfoFactory keyInfoFactory = sigFactory.getKeyInfoFactory();
		List<Object> x509Content = new ArrayList<Object>();
		x509Content.add(cert.getSubjectX500Principal().getName() + "|TGKY="
				+ convertDateToString(new Date(), "DDMMYYYYHH24MISS"));
		x509Content.add(cert);
		X509Data x509Data = keyInfoFactory.newX509Data(x509Content);
		KeyValue keyValue = keyInfoFactory.newKeyValue(publicKey);
		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Arrays.asList(keyValue,
				x509Data));

		DOMSignContext dsc = new DOMSignContext(privateKey,
				doc.getDocumentElement());
		dsc.setURIDereferencer(new URIDereferencer() {
			@Override
			public Data dereference(URIReference uriReference,
					XMLCryptoContext context) throws URIReferenceException {
				final String providerName = System.getProperty(
						"jsr105Provider",
						"org.jcp.xml.dsig.internal.dom.XMLDSigRI");

				XMLSignatureFactory fac = null;
				try {
					fac = XMLSignatureFactory.getInstance("DOM",
							(Provider) Class.forName(providerName)
									.newInstance());
				} catch (Exception e) {
					e.printStackTrace();
				}

				Data data = fac.getURIDereferencer().dereference(uriReference,
						context);
				return data;
			}
		});

		Element dateTimeStampElement = doc.createElement("DateTimeStamp");
		dateTimeStampElement.setTextContent(convertDateToString(new Date(), "DDMMYYYYHH24MISS"));
		//Node node = doc.createTextNode();
		XMLStructure content = new DOMStructure(dateTimeStampElement);
		SignatureProperty sp = sigFactory.newSignatureProperty(Collections.singletonList(content),
				"signatureProperties", "TimeSignatureNHTM");
		SignatureProperties listSp = sigFactory.newSignatureProperties(Collections.singletonList(sp), null);
		XMLObject obj = sigFactory.newXMLObject(Collections.singletonList(listSp),
				null, null, null);
		
		XMLSignature signature = sigFactory
				.newXMLSignature(signedInfo, keyInfo, Collections.singletonList(obj), null, null);
		signature.sign(dsc);

		// System.setProperty("javax.xml.transform.TransformerFactory",
		// "com.sun.org.apache.xalan.internal.xsltc.trax.TransformerFactoryImpl");
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Transformer trans = TransformerFactory.newInstance().newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(os));
		os.flush();
		return os.toByteArray();
	}

	
	//	14/09/17 Cuongdt
	//	Adding new method for SHA1 and SHA-RSA1 security method
	public byte[] signXMLFileXPathGIPSHA1(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath) throws Exception {
		SignatureMethod sm = sigFactory.newSignatureMethod(SignatureMethod.RSA_SHA1, null);
		DigestMethod dm = sigFactory.newDigestMethod(DigestMethod.SHA1, null); 
		return signXMLFileXPathGIP(fileData, privateKey, certificateChain, tagPath, sm, dm);
	}
	
	//	14/09/17 Cuongdt
	//	Adding new method for SHA256 and SHA-RSA256 security method
	public byte[] signXMLFileXPathGIPSHA256(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath) throws Exception {
		DigestMethod dm = sigFactory.newDigestMethod(DigestMethod.SHA256, null); 
		SignatureMethod sm = sigFactory.newSignatureMethod("http://www.w3.org/2001/04/xmldsig-more#rsa-sha256", null);
		return signXMLFileXPathGIP(fileData, privateKey, certificateChain, tagPath, sm, dm);
	}
	
	//	14/09/17 Cuongdt
	//	Adding more two parameters for SignatureMethod and DigestMethod
	//	Extending SHA and RSA security method configurations	
	public byte[] signXMLFileXPathGIP(byte[] fileData, PrivateKey privateKey, Certificate[] certificateChain,
			String tagPath, SignatureMethod sm, DigestMethod dm) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		Document doc = dbFactory.newDocumentBuilder().parse(
				new ByteArrayInputStream(fileData));

//		String providerName = System.getProperty("jsr106Provider", "org.jcp.xml.dsig.internal.dom.XMLDSigRI");
//		final XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance("DOM", (Provider) Class.forName(providerName).newInstance());
		
		String referenceURI = null;
		//XPathExpression expr = null;
		//NodeList nodes;
		// List transforms = null;
		List<Transform> listTransform = null;
		TransformParameterSpec param = null;

		final String path = tagPath;
		referenceURI = "";
		Transform transform = null;
		listTransform = Collections
				.synchronizedList(new ArrayList<Transform>());
		transform = sigFactory.newTransform(Transform.XPATH,
				new XPathFilterParameterSpec(path));
		listTransform.add(transform);

		X509Certificate cert = (X509Certificate) certificateChain[0];
		PublicKey publicKey = cert.getPublicKey();

		Reference ref = sigFactory.newReference(referenceURI, dm, listTransform, null, null);

		SignedInfo signedInfo = sigFactory.newSignedInfo(sigFactory
				.newCanonicalizationMethod(
						CanonicalizationMethod.EXCLUSIVE_WITH_COMMENTS,
						(C14NMethodParameterSpec) param), sm,
				Collections.singletonList(ref));

		KeyInfoFactory keyInfoFactory = sigFactory.getKeyInfoFactory();
		List<Object> x509Content = new ArrayList<Object>();
		x509Content.add(cert.getSubjectX500Principal().getName());
		x509Content.add(cert);
		X509Data x509Data = keyInfoFactory.newX509Data(x509Content);
		KeyValue keyValue = keyInfoFactory.newKeyValue(publicKey);
		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Arrays.asList(keyValue,
				x509Data));

		DOMSignContext dsc = new DOMSignContext(privateKey,
				doc.getDocumentElement());
		dsc.setURIDereferencer(new URIDereferencer() {
			@Override
			public Data dereference(URIReference uriReference,
					XMLCryptoContext context) throws URIReferenceException {
				final String providerName = System.getProperty(
						"jsr105Provider",
						"org.jcp.xml.dsig.internal.dom.XMLDSigRI");

				XMLSignatureFactory fac = null;
				try {
					fac = XMLSignatureFactory.getInstance("DOM",
							(Provider) Class.forName(providerName)
									.newInstance());
				} catch (Exception e) {
					e.printStackTrace();
				}

				Data data = fac.getURIDereferencer().dereference(uriReference,
						context);
				return data;
			}
		});

		XMLSignature signature = sigFactory
				.newXMLSignature(signedInfo, keyInfo);
		signature.sign(dsc);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		Transformer trans = TransformerFactory.newInstance().newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(os));
		os.flush();
		return os.toByteArray();
	}

	public byte[] signXMLFileXPath1(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagPath) throws Exception {
		final String path = tagPath;
		// Instantiate the document to be signed
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setNamespaceAware(true);
		Document doc = dbFactory.newDocumentBuilder().parse(
				new ByteArrayInputStream(fileData));

		// prepare signature factory
		String providerName = System.getProperty("jsr105Provider",
				"org.jcp.xml.dsig.internal.dom.XMLDSigRI");

		final XMLSignatureFactory sigFactory = XMLSignatureFactory.getInstance(
				"DOM", (Provider) Class.forName(providerName).newInstance());
		String referenceURI = null;
		XPathExpression expr = null;
		NodeList nodes;
		List<Transform> transforms = null;

		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		// Find the node to be signed by PATH
		expr = xpath.compile(path);
		nodes = (NodeList) expr.evaluate(doc, XPathConstants.NODESET);
		if (nodes.getLength() < 1) {
			throw new Exception("Không tồn tại path cần ký.");
		}
		referenceURI = ""; // Empty string means whole document
		transforms = new ArrayList<Transform>() {
			private static final long serialVersionUID = -8164254856670035958L;

			{
				add(sigFactory.newTransform(Transform.XPATH,
						new XPathFilterParameterSpec(path)));
				add(sigFactory.newTransform(Transform.ENVELOPED,
						(TransformParameterSpec) null));
			}
		};

		X509Certificate cert = (X509Certificate) certificateChain[0];
		// Create a Reference to the enveloped document
		Reference ref = sigFactory.newReference(referenceURI,
				sigFactory.newDigestMethod(DigestMethod.SHA1, null),
				transforms, null, null);

		// Create the SignedInfo
		SignedInfo signedInfo = sigFactory.newSignedInfo(sigFactory
				.newCanonicalizationMethod(
						CanonicalizationMethod.INCLUSIVE_WITH_COMMENTS,
						(C14NMethodParameterSpec) null), sigFactory
				.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
				Collections.singletonList(ref));

		// Create a KeyValue containing the RSA PublicKey
		KeyInfoFactory keyInfoFactory = sigFactory.getKeyInfoFactory();
		List<Object> x509Content = new ArrayList<Object>();
		x509Content.add(cert.getSubjectX500Principal().getName() + "|TGKY="
				+ convertDateToString(new Date(), "DDMMYYYYHH24MISS"));
		// x509Content.add(cert.getSubjectX500Principal().getName()+"|TGKY="+IHTKKUtils.convertDateToString(new
		// Date(), "DD/MM/YYYY HH:MI:SS"));
		x509Content.add(cert);
		X509Data xd = keyInfoFactory.newX509Data(x509Content);
		// Create a KeyInfo and add the KeyValue to it
		KeyInfo keyInfo = keyInfoFactory.newKeyInfo(Collections
				.singletonList(xd));
		// Create a DOMSignContext and specify the RSA PrivateKey and
		// location of the resulting XMLSignature's parent element
		DOMSignContext dsc = new DOMSignContext(privateKey,
				doc.getDocumentElement());

		// Create the XMLSignature (but don't sign it yet)
		XMLSignature signature = sigFactory
				.newXMLSignature(signedInfo, keyInfo);

		// Marshal, generate (and sign) the enveloped signature
		signature.sign(dsc);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Result result = new StreamResult(out);
		TransformerFactory factory1 = TransformerFactory.newInstance();
		Transformer transformer = factory1.newTransformer();
		transformer.transform(new DOMSource(doc), result);
		return out.toByteArray();
	}

	public byte[] signXMLFile(byte[] fileData, PrivateKey privateKey,
			Certificate[] certificateChain, String tagRef, String tagSign)
			throws Exception {

		// Create a DOM XMLSignatureFactory that will be used to generate the
		// enveloped signature
		XMLSignatureFactory fac = XMLSignatureFactory.getInstance("DOM");

		// Create a Reference to the enveloped document (in this case we are
		// signing the whole document, so a URI of "" signifies that) and
		// also specify the SHA1 digest algorithm and the ENVELOPED Transform.
		String ndungSign = "";
		if (tagRef != null && !tagRef.equalsIgnoreCase("")) {
			ndungSign = "#" + tagRef;
		}
		Reference ref = fac.newReference(ndungSign, fac.newDigestMethod(
				DigestMethod.SHA1, null), Collections.singletonList(fac
				.newTransform(Transform.ENVELOPED,
						(TransformParameterSpec) null)), null, null);
		// Create the SignedInfo
		SignedInfo si = fac
				.newSignedInfo(fac.newCanonicalizationMethod(
						CanonicalizationMethod.INCLUSIVE_WITH_COMMENTS,
						(C14NMethodParameterSpec) null), fac
						.newSignatureMethod(SignatureMethod.RSA_SHA1, null),
						Collections.singletonList(ref));
		X509Certificate certChain = (X509Certificate) certificateChain[0];
		KeyInfoFactory kif = fac.getKeyInfoFactory();
		List<Object> x509Content = new ArrayList<Object>();
		x509Content.add(certChain.getSubjectX500Principal().getName());
		x509Content.add(certChain);
		X509Data xd = kif.newX509Data(x509Content);
		// Create a KeyInfo and add the KeyValue to it
		KeyInfo ki = kif.newKeyInfo(Collections.singletonList(xd));

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		Document doc = dbf.newDocumentBuilder().parse(
				new ByteArrayInputStream(fileData));
		// Create a DOMSignContext and specify the DSA PrivateKey and
		// location of the resulting XMLSignature's parent element
		Node elmSign = null;
		if (tagSign != null && !tagSign.equalsIgnoreCase("")) {
			elmSign = doc.getElementsByTagName(tagSign).item(0);
		} else
			elmSign = doc.getDocumentElement();
		DOMSignContext dsc = new DOMSignContext(privateKey, elmSign);
		// Create the XMLSignature (but don't sign it yet)
		XMLSignature signature = fac.newXMLSignature(si, ki);
		// Marshal, generate (and sign) the enveloped signature
		signature.sign(dsc);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		Result result = new StreamResult(out);
		TransformerFactory factory = TransformerFactory.newInstance();
		Transformer transformer = factory.newTransformer();
		transformer.transform(new DOMSource(doc), result);
		return out.toByteArray();
	}

	public static String convertDateToString(java.util.Date d, String format) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		return convertDateToString(c, format);
	}

	public static String convertDateToString(Calendar d, String format) {
		String dd = Integer.toString(d.get(Calendar.DATE));
		String mm = Integer.toString(d.get(Calendar.MONTH) + 1);
		String yyyy = Integer.toString(d.get(Calendar.YEAR));
		String hh = Integer.toString(d.get(Calendar.HOUR_OF_DAY));
		String mi = Integer.toString(d.get(Calendar.MINUTE));
		String ss = Integer.toString(d.get(Calendar.SECOND));
		String ms = Integer.toString(d.get(Calendar.MILLISECOND));

		if (dd.length() == 1) {
			dd = "0" + dd;
		}
		if (mm.length() == 1) {
			mm = "0" + mm;
		}
		if (hh.length() == 1) {
			hh = "0" + hh;
		}
		if (mi.length() == 1) {
			mi = "0" + mi;
		}
		if (ss.length() == 1) {
			ss = "0" + ss;
		}
		if (ms.length() == 1) {
			ms = "0" + ms;
		}
		if ("DD".equalsIgnoreCase(format)) {
			return dd;
		} else if ("MM".equalsIgnoreCase(format)) {
			return mm;
		} else if ("YYYY".equalsIgnoreCase(format)) {
			return yyyy;
		} else if ("MM/YYYY".equals(format)) {
			return mm + "/" + yyyy;
		} else if ("DD/MM/YYYY".equals(format)) {
			return dd + "/" + mm + "/" + yyyy;
		} else if ("DD/MM/YYYY HH:MI:SS".equals(format)) {
			return dd + "/" + mm + "/" + yyyy + " " + hh + ":" + mi + ":" + ss;
		} else if ("DDMMYYYY HH:MI:SS".equals(format)) {
			return dd + "" + mm + "" + yyyy + " " + hh + ":" + mi + ":" + ss;
		} else if ("DDMMYYYYHH24MISS".equals(format)) {
			return dd + mm + yyyy + hh + mi + ss;
		} else if ("DDMMYYYYHH24MISSMS".equals(format)) {
			return dd + mm + yyyy + hh + mi + ss + ms;
		} else if ("DDMMYYYY".equals(format)) {
			return dd + mm + yyyy;
		} else if ("MMYYYY".equals(format)) {
			return mm + yyyy;
		} else if ("DD-MMM-YYYY HH:MI:SS".equals(format)) {
			SimpleDateFormat format2 = new SimpleDateFormat(
					"dd-MMM-yyyy HH:mm:ss");
			return format2.format(d.getTime());
		} else if ("YYYYMMDDHH24MISSMS".equals(format)) {
			return yyyy + mm + dd + hh + mi + ss + ms;
		} else if ("yyyyDDDHHmmss".equals(format)) {
			SimpleDateFormat format2 = new SimpleDateFormat(format);
			return format2.format(d.getTime());
		}
		return null;
	}

	public static Calendar convertStringToDate(String strDate, String format) {
		String dateValue;
		String timeValue;
		String[] dElement;
		String[] tElement;
		Calendar cal = null;
		if ("DD/MM/YYYY".equals(format)) {
			dElement = strDate.split("/");
			cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, new Integer(dElement[0]).intValue());
			cal.set(Calendar.MONTH, new Integer(dElement[1]).intValue() - 1);
			cal.set(Calendar.YEAR, new Integer(dElement[2]).intValue());
		} else if ("DD/MM/YYYY HH:MI:SS".equals(format)) {
			dateValue = strDate.substring(0, strDate.indexOf(" "));
			timeValue = strDate.substring(strDate.indexOf(" ") + 1);
			dElement = dateValue.split("/");
			tElement = timeValue.split(":");
			cal = Calendar.getInstance();
			cal.set(Calendar.DAY_OF_MONTH, new Integer(dElement[0]).intValue());
			cal.set(Calendar.MONTH, new Integer(dElement[1]).intValue() - 1);
			cal.set(Calendar.YEAR, new Integer(dElement[2]).intValue());
			cal.set(Calendar.HOUR_OF_DAY, new Integer(tElement[0]).intValue());
			cal.set(Calendar.MINUTE, new Integer(tElement[1]).intValue());
			cal.set(Calendar.SECOND, new Integer(tElement[2]).intValue());
		}

		return cal;
	}

	public static Calendar convertStringToDate(String strDate, String format,
			String timezoneID) {
		Calendar cal = convertStringToDate(strDate, format);
		cal.setTimeZone(TimeZone.getTimeZone(timezoneID));
		return cal;
	}

	public static String convertDateToString(Date d) {
		String dStr;
		String yyyy;
		String mm;
		String dd;

		if (d == null)
			return null;

		dStr = d.toString();
		yyyy = dStr.substring(0, 4);
		mm = dStr.substring(5, 7);
		dd = dStr.substring(8);

		return dd + "/" + mm + "/" + yyyy;
	}
}
