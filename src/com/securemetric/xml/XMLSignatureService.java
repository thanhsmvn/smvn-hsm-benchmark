package com.securemetric.xml;

import java.security.PrivateKey;
import java.security.cert.Certificate;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.jce.PKCS11KeyWrapper;
import com.securemetric.jce.PKCS11KeyStoreBean;
import com.securemetric.throwable.SMVNException;

public class XMLSignatureService {

	private static final String KEYSTORE_FILE = "/certificate.pfx";
	private static final String KEYSTORE_PASSWORD = "";
	private static final String VCB_CERTIFICATE = "vcb_certificate";

	private static final Logger LOGGER = LogManager.getLogger(XMLSignatureService.class);

	public byte[] signXML(byte[] xmlBytes, String xpath) {
		try {
			PKCS11KeyWrapper bean = getKeyStoreBean(VCB_CERTIFICATE, KEYSTORE_FILE, KEYSTORE_PASSWORD);
			XMLSigner signer = new XMLSigner();
			byte[] result = signer.signXMLFileXPathExclusiveSHA1(xmlBytes, bean.getPrivateKey(), bean.getCertChain(),
					xpath);
			LOGGER.debug("signXML signXMLFileXPathExclusiveSHA1 successfully! ");
			return result;
		} catch (Exception e) {
			LOGGER.error("signXML error", e);
			return null;
		}

	}

	// 14/09/17 Cuongdt
	// Exposing new method for SHA-RSA256 security method
	public byte[] signXML256(byte[] xmlBytes, String xpath) {
		try {
			PKCS11KeyWrapper bean = getKeyStoreBean(VCB_CERTIFICATE, KEYSTORE_FILE, KEYSTORE_PASSWORD);
			XMLSigner signer = new XMLSigner();
			byte[] result = signer.signXMLFileXPathExclusiveSHA256(xmlBytes, bean.getPrivateKey(), bean.getCertChain(),
					xpath);
			LOGGER.debug("signXML256 signXMLFileXPathExclusiveSHA256 successfully! ");
			return result;
		} catch (Exception e) {
			LOGGER.error("signXML256 error", e);
			return null;
		}
	}

	public byte[] signXMLEnveloped(byte[] xmlBytes, String xpath) {
		try {
			PKCS11KeyWrapper bean = getKeyStoreBean(VCB_CERTIFICATE, KEYSTORE_FILE, KEYSTORE_PASSWORD);
			XMLSigner signer = new XMLSigner();
			byte[] result = signer.signXMLFileXPathEnvelopedSHA1(xmlBytes, bean.getPrivateKey(), bean.getCertChain(),
					xpath);
			LOGGER.debug("signXMLEnveloped signXMLFileXPathEnvelopedSHA1 successfully! ");
			return result;
		} catch (Exception e) {
			LOGGER.error("signXMLEnveloped error", e);
			return null;
		}
	}

	// 14/09/17 Cuongdt
	// Exposing new method for SHA-RSA256 security method
	public byte[] signXMLEnveloped256(byte[] xmlBytes, String xpath) {
		try {
			PKCS11KeyWrapper keyStore = getKeyStoreBean(VCB_CERTIFICATE, KEYSTORE_FILE, KEYSTORE_PASSWORD);
			XMLSigner signer = new XMLSigner();
			byte[] result = signer.signXMLFileXPathEnvelopedSHA256(xmlBytes, keyStore.getPrivateKey(),
					keyStore.getCertChain(), xpath);
			LOGGER.debug("signXMLEnveloped256 signXMLFileXPathEnvelopedSHA256 successfully! ");
			return result;
		} catch (Exception e) {
			LOGGER.error("signXMLEnveloped256 error", e);
			return null;
		}
	}

	private PKCS11KeyWrapper getKeyStoreBean(String p11Id, String keyStorePath, String keyStorePassword)
			throws Exception {
		LOGGER.debug("Getting keyStore bean");
		try {
			PKCS11KeyStoreBean keyStore = PKCS11KeyStoreBean.getInstance();
			PrivateKey privateKey = keyStore.getPrivateKey(p11Id);
			Certificate[] certChain = keyStore.getCertChain(p11Id);
			LOGGER.debug("getKeyStoreBean - from HSM successfully! ");
			return new PKCS11KeyWrapper(privateKey, certChain);

		} catch (SMVNException e) {
			LOGGER.error("getKeyStoreBean ", e);
			LOGGER.debug("getKeyStoreBean - Signing by p12 file");
			return PKCS11KeyWrapper.getKeyStore(keyStorePath, keyStorePassword);
		}
	}
}
