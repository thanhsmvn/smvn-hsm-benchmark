package com.securemetric.xml;

import com.securemetric.jce.JCESpecification;
import com.securemetric.throwable.SMVNException;

public class XMLSpecification extends JCESpecification {

	public XMLSpecification(byte[] data) throws SMVNException {
		super(data);
	}

}
