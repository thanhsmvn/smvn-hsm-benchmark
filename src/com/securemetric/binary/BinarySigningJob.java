package com.securemetric.binary;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Signature;
import java.security.SignatureException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.jce.JCESpecification;

public class BinarySigningJob extends CommonSigningJob {

	private static final Logger LOGGER = LogManager.getLogger(BinarySigningJob.class);

	private final JCESpecification spec;

	public BinarySigningJob(int numberOfInterval, JCESpecification spec) {
		super(numberOfInterval);
		this.spec = spec;
	}

	@Override
	public void process() {
		try {
			Signature sig = Signature.getInstance("SHA1WithRSA");
			sig.initSign(this.spec.getPrivateKey());
			sig.update(this.spec.getData());
			for (int i = 0; i < this.getNumberOfInterval(); i++) {
				sig.sign();
			}

		} catch (NoSuchAlgorithmException e) {
			LOGGER.error(e);
		} catch (SignatureException e) {
			LOGGER.error(e);
		} catch (InvalidKeyException e) {
			LOGGER.error(e);
		}
	}

}
