package com.securemetric.binary;

import java.util.concurrent.CountDownLatch;

import com.securemetric.benchmark.BenchmarkExecutor;
import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.jce.JCESpecification;
import com.securemetric.throwable.SMVNException;

public class BinaryExecutor extends BenchmarkExecutor {
	private final JCESpecification spec;

	public BinaryExecutor(int numberOfThread, int numberOfInterval) throws SMVNException {
		super(numberOfThread, numberOfInterval);
		this.spec = new JCESpecification("abc1234567890qwertas".getBytes());
	}

	@Override
	public CommonSigningJob getSinginJob(int currentThread) {
		return new BinarySigningJob(currentThread, this.spec);
	}

}
