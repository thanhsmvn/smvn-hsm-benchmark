package com.securemetric;

import java.util.Objects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.benchmark.BenchmarkExecutor;
import com.securemetric.benchmark.ExecutorFactory;
import com.securemetric.throwable.SMVNException;

public class HSMBenchMarking {

	private static final Logger LOGGER = LogManager.getLogger(HSMBenchMarking.class);

	public static void main(String[] args) throws Exception {
		try {
			BenchmarkExecutor executor = ExecutorFactory.getInstance().getExecutor();
			if (Objects.isNull(executor)) {
				LOGGER.error("Operation not supported, check sign_type in benchmark.properties");
			}
			executor.execute();
		} catch (SMVNException e) {
			LOGGER.error(e);
		}
	}

}
