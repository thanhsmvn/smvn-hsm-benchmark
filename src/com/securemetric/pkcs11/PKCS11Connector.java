package com.securemetric.pkcs11;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.RSAPublicKeySpec;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.UUID;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.util.Strings;

import com.securemetric.throwable.SMVNException;

import iaik.pkcs.pkcs11.wrapper.CK_ATTRIBUTE;
import iaik.pkcs.pkcs11.wrapper.CK_MECHANISM;
import iaik.pkcs.pkcs11.wrapper.PKCS11;
import iaik.pkcs.pkcs11.wrapper.PKCS11Constants;
import iaik.pkcs.pkcs11.wrapper.PKCS11Exception;
import sun.security.util.DerOutputStream;
import sun.security.x509.AlgorithmId;
import sun.security.x509.X500Name;

public class PKCS11Connector {

	private static final Logger LOGGER = LogManager.getLogger(PKCS11Connector.class);
	private final String tokenPIN;
	private final int slotId;
	private final PKCS11 pkcs11Module;

	public PKCS11Connector() throws SMVNException {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("hsm_config.properties"));
			String libraryPath = properties.getProperty("library_path");
			LOGGER.debug("PKCS11Connector - library_path = " + libraryPath);
			this.tokenPIN = properties.getProperty("token_pin", "0");
			String slotIdStr = properties.getProperty("slot_id", "0");
			LOGGER.debug("PKCS11Connector - slot_id = " + slotIdStr);
			this.slotId = Integer.parseInt(slotIdStr);
			this.pkcs11Module = iaik.pkcs.pkcs11.wrapper.PKCS11Connector.connectToPKCS11Module(libraryPath);
			pkcs11Module.C_Initialize(null, true);
		} catch (Exception e) {
			LOGGER.error("Cannot construct PKCS11Connector", e);
			throw new SMVNException(e);
		}
	}

	public List<Long> openSessions(int numberOfThread) throws SMVNException {
		long flags = PKCS11Constants.CKF_RW_SESSION;
		flags |= PKCS11Constants.CKF_SERIAL_SESSION;
		List<Long> sessions = new ArrayList<Long>();
		for (int i = 0; i < numberOfThread; i++) {
			try {
				sessions.add(pkcs11Module.C_OpenSession(this.slotId, flags, null, null));
			} catch (PKCS11Exception e) {
				LOGGER.warn(e);
			}
		}
		return sessions;
	}

	public long[] findRSAPrivateKey(long session) throws SMVNException {
		return this.findRSAPrivateKey(session, "");
	}

	public long[] findRSAPrivateKey(long session, String keyName) throws SMVNException {
		List<CK_ATTRIBUTE> privateKeys = new ArrayList<CK_ATTRIBUTE>();
		CK_ATTRIBUTE keyType = new CK_ATTRIBUTE();
		keyType.type = PKCS11Constants.CKA_KEY_TYPE;
		keyType.pValue = PKCS11Constants.CKK_RSA;
		privateKeys.add(keyType);

		CK_ATTRIBUTE clazz = new CK_ATTRIBUTE();
		clazz.type = PKCS11Constants.CKA_CLASS;
		clazz.pValue = PKCS11Constants.CKO_PRIVATE_KEY;
		privateKeys.add(clazz);

		CK_ATTRIBUTE privAttr = new CK_ATTRIBUTE();
		privAttr.type = PKCS11Constants.CKA_PRIVATE;
		privAttr.pValue = PKCS11Constants.TRUE;
		privateKeys.add(privAttr);

		CK_ATTRIBUTE signAttr = new CK_ATTRIBUTE();
		signAttr.type = PKCS11Constants.CKA_SIGN;
		signAttr.pValue = PKCS11Constants.TRUE;
		privateKeys.add(signAttr);

		if (Objects.nonNull(keyName) && !Strings.isEmpty(keyName)) {
			CK_ATTRIBUTE labelAttr = new CK_ATTRIBUTE();
			labelAttr.type = PKCS11Constants.CKA_LABEL;
			labelAttr.pValue = keyName;
			privateKeys.add(labelAttr);

		}
		CK_ATTRIBUTE privateKeyTemplate[] = new CK_ATTRIBUTE[privateKeys.size()];
		privateKeys.toArray(privateKeyTemplate);

		try {
			pkcs11Module.C_FindObjectsInit(session, privateKeyTemplate, true);
			long[] wrapperKeys = pkcs11Module.C_FindObjects(session, 1);
			pkcs11Module.C_FindObjectsFinal(session);
			return wrapperKeys;
		} catch (PKCS11Exception e) {
			throw new SMVNException();
		}
	}

	public long[] findRSAPublicKey(long session) throws SMVNException {
		return this.findRSAPublicKey(session, "");
	}

	public long[] findRSAPublicKey(long session, String keyName) throws SMVNException {
		List<CK_ATTRIBUTE> publicKeys = new ArrayList<CK_ATTRIBUTE>();
		CK_ATTRIBUTE keyTypeAttr = new CK_ATTRIBUTE();
		keyTypeAttr.type = PKCS11Constants.CKA_KEY_TYPE;
		keyTypeAttr.pValue = PKCS11Constants.CKK_RSA;
		publicKeys.add(keyTypeAttr);

		CK_ATTRIBUTE clazzAttr = new CK_ATTRIBUTE();
		clazzAttr.type = PKCS11Constants.CKA_CLASS;
		clazzAttr.pValue = PKCS11Constants.CKO_PUBLIC_KEY;
		publicKeys.add(clazzAttr);

		if (!Strings.isEmpty(keyName)) {
			CK_ATTRIBUTE labelAttr = new CK_ATTRIBUTE();
			labelAttr.type = PKCS11Constants.CKA_LABEL;
			labelAttr.pValue = keyName.getBytes();
			publicKeys.add(labelAttr);
		}
		CK_ATTRIBUTE publicKeyTemplate[] = new CK_ATTRIBUTE[publicKeys.size()];
		publicKeys.toArray(publicKeyTemplate);
		try {
			pkcs11Module.C_FindObjectsInit(session, publicKeyTemplate, true);
			long[] wrapperKeys = pkcs11Module.C_FindObjects(session, 100);
			pkcs11Module.C_FindObjectsFinal(session);
			return wrapperKeys;
		} catch (PKCS11Exception e) {
			throw new SMVNException(e);
		}

	}

	public String generateRSAKeyPair(long session) throws SMVNException {
		return this.generateRSAKeyPair(session, UUID.randomUUID().toString());
	}

	public String generateRSAKeyPair(long session, String keyName) throws SMVNException {
		CK_ATTRIBUTE[] privateKeys = getPrivateKeyAttributes(keyName);

		CK_ATTRIBUTE[] publicKeyAttrs = getPublicKeyAttributes(keyName);

		CK_MECHANISM ck_MECHANISM = new CK_MECHANISM();
		ck_MECHANISM.mechanism = PKCS11Constants.CKM_RSA_PKCS_KEY_PAIR_GEN;
		ck_MECHANISM.pParameter = null;

		long[] keyPairs;
		try {
			keyPairs = pkcs11Module.C_GenerateKeyPair(session, ck_MECHANISM, publicKeyAttrs, privateKeys, true);
			String generateCSR = generateCSR(session, keyPairs);
			System.out.println(generateCSR);
			return generateCSR;
		} catch (PKCS11Exception e) {
			throw new SMVNException(e);
		}

	}

	private CK_ATTRIBUTE[] getPrivateKeyAttributes(String keyName) {
		List<CK_ATTRIBUTE> privateKeys = new ArrayList<CK_ATTRIBUTE>();

		CK_ATTRIBUTE priAttr1 = new CK_ATTRIBUTE();
		priAttr1.type = PKCS11Constants.CKA_KEY_TYPE;
		priAttr1.pValue = PKCS11Constants.CKK_RSA;
		privateKeys.add(priAttr1);

		CK_ATTRIBUTE priAttr2 = new CK_ATTRIBUTE();
		priAttr2.type = PKCS11Constants.CKA_CLASS;
		priAttr2.pValue = PKCS11Constants.CKO_PRIVATE_KEY;
		privateKeys.add(priAttr2);

		CK_ATTRIBUTE priAttr3 = new CK_ATTRIBUTE();
		priAttr3.type = PKCS11Constants.CKA_PRIVATE;
		priAttr3.pValue = PKCS11Constants.TRUE;
		privateKeys.add(priAttr3);

		CK_ATTRIBUTE priAttr4 = new CK_ATTRIBUTE();
		priAttr4.type = PKCS11Constants.CKA_DECRYPT;
		priAttr4.pValue = PKCS11Constants.TRUE;
		privateKeys.add(priAttr4);

		CK_ATTRIBUTE priAttr5 = new CK_ATTRIBUTE();
		priAttr5.type = PKCS11Constants.CKA_LABEL;
		priAttr5.pValue = keyName;
		privateKeys.add(priAttr5);

		CK_ATTRIBUTE priAttr6 = new CK_ATTRIBUTE();
		priAttr6.type = PKCS11Constants.CKA_ID;
		priAttr6.pValue = keyName;
		privateKeys.add(priAttr6);

		CK_ATTRIBUTE priSubject = new CK_ATTRIBUTE();
		priSubject.type = PKCS11Constants.CKA_SUBJECT;
		priSubject.pValue = keyName.getBytes();
		privateKeys.add(priSubject);

		CK_ATTRIBUTE priAttr7 = new CK_ATTRIBUTE();
		priAttr7.type = PKCS11Constants.CKA_SENSITIVE;
		priAttr7.pValue = true;
		privateKeys.add(priAttr7);

		CK_ATTRIBUTE priAttr8 = new CK_ATTRIBUTE();
		priAttr8.type = PKCS11Constants.CKA_TOKEN;
		priAttr8.pValue = PKCS11Constants.TRUE;
		privateKeys.add(priAttr8);

		CK_ATTRIBUTE priExtractable = new CK_ATTRIBUTE();
		priExtractable.type = PKCS11Constants.CKA_EXTRACTABLE;
		priExtractable.pValue = PKCS11Constants.FALSE;
		privateKeys.add(priExtractable);

		CK_ATTRIBUTE priUnwrap = new CK_ATTRIBUTE();
		priUnwrap.type = PKCS11Constants.CKA_UNWRAP;
		priUnwrap.pValue = PKCS11Constants.TRUE;
		privateKeys.add(priUnwrap);

		CK_ATTRIBUTE priSign = new CK_ATTRIBUTE();
		priSign.type = PKCS11Constants.CKA_SIGN;
		priSign.pValue = PKCS11Constants.TRUE;
		privateKeys.add(priSign);

		CK_ATTRIBUTE privateKeyTemplate[] = new CK_ATTRIBUTE[privateKeys.size()];
		privateKeys.toArray(privateKeyTemplate);
		return privateKeyTemplate;
	}

	private CK_ATTRIBUTE[] getPublicKeyAttributes(String keyName) {
		List<CK_ATTRIBUTE> publicKeyAttrs = new ArrayList<CK_ATTRIBUTE>();
		CK_ATTRIBUTE pubAttr1 = new CK_ATTRIBUTE();
		pubAttr1.type = PKCS11Constants.CKA_KEY_TYPE;
		pubAttr1.pValue = PKCS11Constants.CKK_RSA;
		publicKeyAttrs.add(pubAttr1);

		CK_ATTRIBUTE pubAttr2 = new CK_ATTRIBUTE();
		pubAttr2.type = PKCS11Constants.CKA_CLASS;
		pubAttr2.pValue = PKCS11Constants.CKO_PUBLIC_KEY;
		publicKeyAttrs.add(pubAttr2);

		CK_ATTRIBUTE pubAttr3 = new CK_ATTRIBUTE();
		pubAttr3.type = PKCS11Constants.CKA_MODULUS_BITS;
		pubAttr3.pValue = 2048;
		publicKeyAttrs.add(pubAttr3);

		CK_ATTRIBUTE pubAttr5 = new CK_ATTRIBUTE();
		pubAttr5.type = PKCS11Constants.CKA_LABEL;
		pubAttr5.pValue = keyName;
		publicKeyAttrs.add(pubAttr5);

		CK_ATTRIBUTE pubAttr6 = new CK_ATTRIBUTE();
		pubAttr6.type = PKCS11Constants.CKA_ID;
		pubAttr6.pValue = keyName;
		publicKeyAttrs.add(pubAttr6);

		CK_ATTRIBUTE pubSubject = new CK_ATTRIBUTE();
		pubSubject.type = PKCS11Constants.CKA_SUBJECT;
		pubSubject.pValue = keyName.getBytes();
		publicKeyAttrs.add(pubSubject);

		CK_ATTRIBUTE pubAttr7 = new CK_ATTRIBUTE();
		pubAttr7.type = PKCS11Constants.CKA_ENCRYPT;
		pubAttr7.pValue = PKCS11Constants.TRUE;
		publicKeyAttrs.add(pubAttr7);

		CK_ATTRIBUTE pubAttr8 = new CK_ATTRIBUTE();
		pubAttr8.type = PKCS11Constants.CKA_TOKEN;
		pubAttr8.pValue = PKCS11Constants.TRUE;
		publicKeyAttrs.add(pubAttr8);

		CK_ATTRIBUTE pubAttr9 = new CK_ATTRIBUTE();
		pubAttr9.type = PKCS11Constants.CKA_PUBLIC_EXPONENT;
		pubAttr9.pValue = this.decodeUsingBigInteger("010001");
		publicKeyAttrs.add(pubAttr9);

		CK_ATTRIBUTE pubWrap = new CK_ATTRIBUTE();
		pubWrap.type = PKCS11Constants.CKA_WRAP;
		pubWrap.pValue = PKCS11Constants.TRUE;
		publicKeyAttrs.add(pubWrap);

		CK_ATTRIBUTE pubVerify = new CK_ATTRIBUTE();
		pubVerify.type = PKCS11Constants.CKA_VERIFY;
		pubVerify.pValue = PKCS11Constants.TRUE;
		publicKeyAttrs.add(pubVerify);

		CK_ATTRIBUTE pubPrivate = new CK_ATTRIBUTE();
		pubPrivate.type = PKCS11Constants.CKA_PRIVATE;
		pubPrivate.pValue = PKCS11Constants.FALSE;
		publicKeyAttrs.add(pubPrivate);

		CK_ATTRIBUTE publicKeyTemplate[] = new CK_ATTRIBUTE[publicKeyAttrs.size()];
		publicKeyAttrs.toArray(publicKeyTemplate);
		return publicKeyTemplate;
	}

	public byte[] decodeUsingBigInteger(String hexString) {
		byte[] byteArray = new BigInteger(hexString, 16).toByteArray();
		if (byteArray[0] == 0) {
			byte[] output = new byte[byteArray.length - 1];
			System.arraycopy(byteArray, 1, output, 0, output.length);
			return output;
		}
		return byteArray;
	}

	private String generateCSR(long session, long[] keyPairs) throws PKCS11Exception, SMVNException {
		List<CK_ATTRIBUTE> retrieveValueAttr = new ArrayList<CK_ATTRIBUTE>();
		CK_ATTRIBUTE keyType = new CK_ATTRIBUTE();
		keyType.type = PKCS11Constants.CKA_KEY_TYPE;
		retrieveValueAttr.add(keyType);

		CK_ATTRIBUTE keyModulus = new CK_ATTRIBUTE();
		keyModulus.type = PKCS11Constants.CKA_MODULUS;
		retrieveValueAttr.add(keyModulus);

		CK_ATTRIBUTE keypubExp = new CK_ATTRIBUTE();
		keypubExp.type = PKCS11Constants.CKA_PUBLIC_EXPONENT;
		retrieveValueAttr.add(keypubExp);

		CK_ATTRIBUTE[] attrValue = new CK_ATTRIBUTE[retrieveValueAttr.size()];
		retrieveValueAttr.toArray(attrValue);

		pkcs11Module.C_GetAttributeValue(session, keyPairs[0], attrValue, true);

		RSAPublicKeySpec publicSpec = new RSAPublicKeySpec(new BigInteger(1, (byte[]) attrValue[1].pValue),
				new BigInteger(1, (byte[]) attrValue[2].pValue));
		KeyFactory factory;
		try {
			factory = KeyFactory.getInstance("RSA");
			PublicKey rsaPublicKey = factory.generatePublic(publicSpec);
			X500Name x500Name = new X500Name("CN=Test,OU=Test,O=Test,L=Test,S=Test,C=Test");
			byte[] certReqInfo = createCertificationRequestInfo(x500Name, rsaPublicKey);
			CK_MECHANISM ckMech = new CK_MECHANISM();
			ckMech.mechanism = PKCS11Constants.CKM_SHA1_RSA_PKCS;
			ckMech.pParameter = null;
			byte[] signature = this.sign(session, keyPairs[1], ckMech, certReqInfo);
			byte[] csrDEREncoded = this.createCertificationRequestValue(certReqInfo, "SHA1WithRSA", signature);

			return createPEMFormat(csrDEREncoded);
		} catch (NoSuchAlgorithmException | InvalidKeySpecException | IOException e) {
			throw new SMVNException();
		}
	}

	private String createPEMFormat(byte[] data) {
		final ByteArrayOutputStream out = new ByteArrayOutputStream();
		final PrintStream ps = new PrintStream(out);
		ps.println("-----BEGIN NEW CERTIFICATE REQUEST-----");
		ps.println(Base64.getMimeEncoder().encodeToString(data));
		ps.println("-----END NEW CERTIFICATE REQUEST-----");
		return out.toString();
	}

	private byte[] createCertificationRequestValue(byte[] certReqInfo, String signAlgo, byte[] signature)
			throws IOException, NoSuchAlgorithmException {
		final DerOutputStream der1 = new DerOutputStream();
		der1.write(certReqInfo);

		// add signature algorithm identifier, and a digital signature on the
		// certification request
		// information
		AlgorithmId.get(signAlgo).encode(der1);
		der1.putBitString(signature);

		// final DER encoded output
		final DerOutputStream der2 = new DerOutputStream();
		der2.write((byte) 48, der1);
		return der2.toByteArray();
	}

	public byte[] encrypt(long session, String keyName) throws SMVNException {
		long[] pubKeys = this.findRSAPublicKey(session, keyName);
		if (pubKeys.length == 0) {
			LOGGER.info("No public key found with label" + keyName);
			return null;
		}

		CK_MECHANISM ckMech = new CK_MECHANISM();
		ckMech.mechanism = PKCS11Constants.CKM_RSA_PKCS;
		ckMech.pParameter = null;
		try {
			pkcs11Module.C_EncryptInit(session, ckMech, pubKeys[0], true);
			byte[] c_Encrypt = pkcs11Module.C_Encrypt(session, "abc".getBytes());
			System.out.println("Encrypted done!, resutl = " + Base64.getEncoder().encodeToString(c_Encrypt));
			return c_Encrypt;
		} catch (PKCS11Exception e) {
			throw new SMVNException(e);
		} finally {
//			pkcs11Module.C_EncryptFinal(session);
		}
	}

	public byte[] decrypt(long session, byte[] dataEcrypted, String keyName) throws SMVNException {
		long[] pubKeys = this.findRSAPrivateKey(session, keyName);
		if (pubKeys.length == 0) {
			LOGGER.info("No private key found with label " + keyName);
			return null;
		}
		CK_MECHANISM ckMech = new CK_MECHANISM();
		ckMech.mechanism = PKCS11Constants.CKM_RSA_PKCS;
		ckMech.pParameter = null;
		try {
			pkcs11Module.C_DecryptInit(session, ckMech, pubKeys[0], true);
			byte[] c_Decrypt = pkcs11Module.C_Decrypt(session, dataEcrypted);
			System.out.println("Decrypted data=" + new String(c_Decrypt));
			return c_Decrypt;
		} catch (PKCS11Exception e) {
			throw new SMVNException(e);
		} finally {
//			pkcs11Module.C_DecryptFinal(session);
		}
	}

	private byte[] createCertificationRequestInfo(X500Name x500Name, PublicKey publicKey) throws IOException {
		final DerOutputStream der1 = new DerOutputStream();
		der1.putInteger(BigInteger.ZERO);
		x500Name.encode(der1);
		der1.write(publicKey.getEncoded());

		// der encoded certificate request info
		final DerOutputStream der2 = new DerOutputStream();
		der2.write((byte) 48, der1);
		return der2.toByteArray();
	}

	public byte[] sign(long session, long privateKey, CK_MECHANISM ck_MECHANISM, byte[] data) throws SMVNException {
		try {
			pkcs11Module.C_SignInit(session, ck_MECHANISM, privateKey, true);
			return pkcs11Module.C_Sign(session, data);
		} catch (PKCS11Exception e) {
			LOGGER.error(e);
			throw new SMVNException(e);
		}

	}

	public PKCS11 getPkcs11Module() {
		return pkcs11Module;
	}

	public void doLogin(Long session) throws SMVNException {
		try {
			pkcs11Module.C_Login(session, PKCS11Constants.CKU_USER, this.tokenPIN.toCharArray(), true);
		} catch (PKCS11Exception e) {
			LOGGER.error(e);
			throw new SMVNException(e);
		}

	}

	public void doLogout(Long session) {
		try {
			this.pkcs11Module.C_Logout(session);
		} catch (PKCS11Exception e) {
			LOGGER.error(e);
		}
	}

	public void doCloseSession(long session) {
		try {
			this.pkcs11Module.C_CloseSession(session);
		} catch (PKCS11Exception e) {
			LOGGER.error(e);
		}

	}

	public void getPrivateKeyAttr(long session, String keyName) throws SMVNException {
		long[] findRSAPrivateKey = this.findRSAPrivateKey(session);
		LOGGER.info("length = " + findRSAPrivateKey.length);
		if (findRSAPrivateKey.length == 0) {
			LOGGER.info("No private key found with name = " + keyName);
			return;
		}
		List<CK_ATTRIBUTE> retrieveValueAttr = new ArrayList<CK_ATTRIBUTE>();
		CK_ATTRIBUTE ckaClass = new CK_ATTRIBUTE();
		ckaClass.type = PKCS11Constants.CKA_CLASS;
		retrieveValueAttr.add(ckaClass);

		CK_ATTRIBUTE ckaID = new CK_ATTRIBUTE();
		ckaID.type = PKCS11Constants.CKA_ID;
		retrieveValueAttr.add(ckaID);

		CK_ATTRIBUTE ckaLabel = new CK_ATTRIBUTE();
		ckaLabel.type = PKCS11Constants.CKA_LABEL;
		retrieveValueAttr.add(ckaLabel);

		CK_ATTRIBUTE ckaEncrypt = new CK_ATTRIBUTE();
		ckaEncrypt.type = PKCS11Constants.CKA_ENCRYPT;
		retrieveValueAttr.add(ckaEncrypt);

		CK_ATTRIBUTE ckaDecrypt = new CK_ATTRIBUTE();
		ckaDecrypt.type = PKCS11Constants.CKA_DECRYPT;
		retrieveValueAttr.add(ckaDecrypt);

		CK_ATTRIBUTE[] attrValue = new CK_ATTRIBUTE[retrieveValueAttr.size()];
		retrieveValueAttr.toArray(attrValue);

		try {
			pkcs11Module.C_GetAttributeValue(session, findRSAPrivateKey[0], attrValue, true);
			System.out.println(String.format("ID = %s, Label = %s, Class = %s, Encrypted= %s, Decrypted = %s",
					new String((byte[]) attrValue[1].pValue), new String((char[]) attrValue[2].pValue),
					String.valueOf(attrValue[0].pValue), String.valueOf(attrValue[3].pValue),
					String.valueOf(attrValue[4].pValue)));
		} catch (PKCS11Exception e) {
			throw new SMVNException(e);
		}

	}

	public void getAllObjects(long session) throws SMVNException {
		CK_ATTRIBUTE certificateAttr[] = new CK_ATTRIBUTE[0];
		try {
//			long[] objects = this.findRSAPrivateKey(session);
			pkcs11Module.C_FindObjectsInit(session, certificateAttr, true);
			long[] objects = pkcs11Module.C_FindObjects(session, 100);
			pkcs11Module.C_FindObjectsFinal(session);
			for (long object : objects) {
				List<CK_ATTRIBUTE> retrieveValueAttr = new ArrayList<CK_ATTRIBUTE>();
				CK_ATTRIBUTE ckaClass = new CK_ATTRIBUTE();
				ckaClass.type = PKCS11Constants.CKA_CLASS;
				retrieveValueAttr.add(ckaClass);

				CK_ATTRIBUTE ckaID = new CK_ATTRIBUTE();
				ckaID.type = PKCS11Constants.CKA_ID;
				retrieveValueAttr.add(ckaID);

				CK_ATTRIBUTE ckaEncrypt = new CK_ATTRIBUTE();
				ckaEncrypt.type = PKCS11Constants.CKA_ENCRYPT;
				retrieveValueAttr.add(ckaEncrypt);

				CK_ATTRIBUTE ckaDecrypt = new CK_ATTRIBUTE();
				ckaDecrypt.type = PKCS11Constants.CKA_DECRYPT;
				retrieveValueAttr.add(ckaDecrypt);

				CK_ATTRIBUTE[] attrValue = new CK_ATTRIBUTE[retrieveValueAttr.size()];
				retrieveValueAttr.toArray(attrValue);

				pkcs11Module.C_GetAttributeValue(session, object, attrValue, true);

				Object id = attrValue[1].pValue;
				Object clazz = attrValue[0].pValue;
				Object encrypted = attrValue[2].pValue;
				Object decrypted = attrValue[3].pValue;
				System.out.println(String.format("ID = %s, CLASS = %s, DECRYPT ==== %s", new String((byte[]) id),
						String.valueOf(clazz == null ? false : clazz),
						String.valueOf(decrypted == null ? false : decrypted)));
			}
		} catch (PKCS11Exception e) {
			throw new SMVNException(e);
		}

	}

	public void importCert(long session, String cert, String keyName) throws SMVNException {
		if (Objects.isNull(cert) || cert.trim().isEmpty()) {
			throw new SMVNException("cert to import cannot be null or empty");
		}
		try {
			X509Certificate certificateFromBytes = this.getCertificateFromBytes(Base64.getDecoder().decode(cert));
			List<CK_ATTRIBUTE> certificates = new ArrayList<CK_ATTRIBUTE>();

			CK_ATTRIBUTE priAttr2 = new CK_ATTRIBUTE();
			priAttr2.type = PKCS11Constants.CKA_CLASS;
			priAttr2.pValue = PKCS11Constants.CKO_CERTIFICATE;
			certificates.add(priAttr2);

			CK_ATTRIBUTE priAttr3 = new CK_ATTRIBUTE();
			priAttr3.type = PKCS11Constants.CKA_CERTIFICATE_TYPE;
			priAttr3.pValue = PKCS11Constants.CKC_X_509;
			certificates.add(priAttr3);

			CK_ATTRIBUTE priAttr4 = new CK_ATTRIBUTE();
			priAttr4.type = PKCS11Constants.CKA_VALUE;
			priAttr4.pValue = certificateFromBytes.getEncoded();
			certificates.add(priAttr4);

			CK_ATTRIBUTE priAttr5 = new CK_ATTRIBUTE();
			priAttr5.type = PKCS11Constants.CKA_LABEL;
			priAttr5.pValue = "testkeygen";
			certificates.add(priAttr5);

			CK_ATTRIBUTE priAttr6 = new CK_ATTRIBUTE();
			priAttr6.type = PKCS11Constants.CKA_ID;
			priAttr6.pValue = keyName;
			certificates.add(priAttr6);

			CK_ATTRIBUTE priAttrSubject = new CK_ATTRIBUTE();
			priAttrSubject.type = PKCS11Constants.CKA_SUBJECT;
			priAttrSubject.pValue = keyName;
			certificates.add(priAttrSubject);

			CK_ATTRIBUTE priAttrIssuer = new CK_ATTRIBUTE();
			priAttrIssuer.type = PKCS11Constants.CKA_ISSUER;
			priAttrIssuer.pValue = certificateFromBytes.getIssuerDN().getName().getBytes();
			certificates.add(priAttrIssuer);

			CK_ATTRIBUTE priAttrSN = new CK_ATTRIBUTE();
			priAttrSN.type = PKCS11Constants.CKA_SERIAL_NUMBER;
			String byteValue = certificateFromBytes.getSerialNumber().toString(16);
			priAttrSN.pValue = byteValue;
			certificates.add(priAttrSN);

			CK_ATTRIBUTE pubAttrToken = new CK_ATTRIBUTE();
			pubAttrToken.type = PKCS11Constants.CKA_TOKEN;
			pubAttrToken.pValue = PKCS11Constants.TRUE;
			certificates.add(pubAttrToken);

			CK_ATTRIBUTE certificateAttr[] = new CK_ATTRIBUTE[certificates.size()];
			certificates.toArray(certificateAttr);

			long c_CreateObject = pkcs11Module.C_CreateObject(session, certificateAttr, true);
			System.out.println(c_CreateObject);
		} catch (PKCS11Exception | CertificateException e) {
			LOGGER.error(e);
			throw new SMVNException(e);
		}

	}

	private X509Certificate getCertificateFromBytes(byte[] certData) throws SMVNException {
		try {
			CertificateFactory certFactory = CertificateFactory.getInstance("X.509");

			ByteArrayInputStream inStream = new ByteArrayInputStream(certData);
			try {
				return (X509Certificate) certFactory.generateCertificate(inStream);
			} finally {
				try {
					inStream.close();
				} catch (IOException e) {
					LOGGER.debug(e);
				}
			}

		} catch (CertificateException e) {
			throw new SMVNException(e);
		}
	}

}
