package com.securemetric.pkcs11;

import java.io.FileInputStream;
import java.util.Properties;

import com.securemetric.throwable.SMVNException;

public class PKCS11KeySpecification {

	private final String keyName;

	public PKCS11KeySpecification() throws SMVNException {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("hsm_config.properties"));
			this.keyName = properties.getProperty("key_name");
		} catch (Exception e) {
			throw new SMVNException(e);
		}
	}

	public String getKeyName() {
		return keyName;
	}

}
