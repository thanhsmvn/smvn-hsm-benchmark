package com.securemetric.pkcs11;

import java.util.List;

import com.securemetric.benchmark.BenchmarkExecutor;
import com.securemetric.throwable.SMVNException;

public abstract class PKCS11Executor extends BenchmarkExecutor {

	private final PKCS11Connector pkcs11Connector;
	private final List<Long> sessions;

	public PKCS11Executor(int numberOfThread, int numberOfInterval) throws SMVNException {
		super(numberOfThread, numberOfInterval);
		this.pkcs11Connector = new PKCS11Connector();
		this.sessions = this.pkcs11Connector.openSessions(numberOfThread);
		this.pkcs11Connector.doLogin(this.sessions.get(0));
	}

	public List<Long> getSessions() {
		return sessions;
	}

	public PKCS11Connector getPkcs11Connector() {
		return pkcs11Connector;
	}

}
