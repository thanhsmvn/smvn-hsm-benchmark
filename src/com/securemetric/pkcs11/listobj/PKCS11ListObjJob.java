package com.securemetric.pkcs11.listobj;

import com.securemetric.pkcs11.PKCS11BasedJob;
import com.securemetric.pkcs11.PKCS11Connector;
import com.securemetric.throwable.SMVNException;

public class PKCS11ListObjJob extends PKCS11BasedJob {
	public PKCS11ListObjJob(int numberOfInterval, PKCS11Connector pkcs11Connector, long session) {
		super(numberOfInterval, pkcs11Connector, session);
	}

	@Override
	public void process() throws SMVNException {
		int numberOfInterval = this.getNumberOfInterval();
		for (int i = 0; i < numberOfInterval; i++) {
			this.getPkcs11Connector().getAllObjects(getSession());
		}
	}

}
