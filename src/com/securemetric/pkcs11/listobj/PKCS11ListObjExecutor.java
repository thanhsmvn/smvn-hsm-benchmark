package com.securemetric.pkcs11.listobj;

import java.util.concurrent.CountDownLatch;

import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.pkcs11.PKCS11Executor;
import com.securemetric.throwable.SMVNException;

public class PKCS11ListObjExecutor extends PKCS11Executor {

	public PKCS11ListObjExecutor(int numberOfThread, int numberOfInterval) throws SMVNException {
		super(numberOfThread, numberOfInterval);
	}

	@Override
	public CommonSigningJob getSinginJob(int currentThread) {
		return new PKCS11ListObjJob(this.getNumberOfInterval(), getPkcs11Connector(),
				(long) this.getSessions().get(currentThread));
	}

}
