package com.securemetric.pkcs11.certimport;

import java.util.concurrent.CountDownLatch;

import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.pkcs11.PKCS11Executor;
import com.securemetric.throwable.SMVNException;

public class PKCS11CertImportExecutor extends PKCS11Executor {

	private final PKCS11CertImportSpecification certImportSpec;

	public PKCS11CertImportExecutor(int numberOfThread, int numberOfInterval) throws SMVNException {
		super(numberOfThread, numberOfInterval);
		this.certImportSpec = new PKCS11CertImportSpecification();
	}

	@Override
	public CommonSigningJob getSinginJob(int currentThread) {
		return new PKCS11CertImportJob(this.getNumberOfInterval(), getPkcs11Connector(),
				(long) this.getSessions().get(currentThread), this.certImportSpec);
	}

}
