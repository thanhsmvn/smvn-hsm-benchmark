package com.securemetric.pkcs11.certimport;

import com.securemetric.pkcs11.PKCS11BasedJob;
import com.securemetric.pkcs11.PKCS11Connector;
import com.securemetric.throwable.SMVNException;

public class PKCS11CertImportJob extends PKCS11BasedJob {

	private final PKCS11CertImportSpecification certImportSpec;

	public PKCS11CertImportJob(int numberOfInterval, PKCS11Connector pkcs11Connector, long session,
			PKCS11CertImportSpecification certImportSpec) {
		super(numberOfInterval, pkcs11Connector, session);
		this.certImportSpec = certImportSpec;
	}

	@Override
	public void process() throws SMVNException {
		for (int i = 0; i < this.getNumberOfInterval(); i++) {
			this.getPkcs11Connector().importCert(this.getSession(), this.certImportSpec.getCert(),
					this.certImportSpec.getKeyName());
		}
	}

}
