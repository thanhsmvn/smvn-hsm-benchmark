package com.securemetric.pkcs11.certimport;

import java.io.FileInputStream;
import java.util.Properties;

import com.securemetric.pkcs11.PKCS11KeySpecification;
import com.securemetric.throwable.SMVNException;

public class PKCS11CertImportSpecification {
	private final String keyName;
	private final String cert;
	public PKCS11CertImportSpecification() throws SMVNException {
		Properties properties = new Properties();
        try {
			properties.load(new FileInputStream("hsm_config.properties"));
			this.cert = properties.getProperty("cert");
			this.keyName = properties.getProperty("key_name");
		} catch (Exception e) {
			throw new SMVNException(e);
		}
	}
	public String getCert() {
		return cert;
	}
	public String getKeyName() {
		return keyName;
	}

}
