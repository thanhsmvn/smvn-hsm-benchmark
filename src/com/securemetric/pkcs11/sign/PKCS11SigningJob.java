package com.securemetric.pkcs11.sign;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.pkcs11.PKCS11BasedJob;
import com.securemetric.pkcs11.PKCS11Connector;
import com.securemetric.pkcs11.PKCS11KeySpecification;
import com.securemetric.throwable.SMVNException;

import iaik.pkcs.pkcs11.wrapper.CK_MECHANISM;
import iaik.pkcs.pkcs11.wrapper.PKCS11Constants;

public class PKCS11SigningJob extends PKCS11BasedJob {

	Logger LOGGER = LogManager.getLogger(PKCS11SigningJob.class);

	private final PKCS11KeySpecification keySpec;

	public PKCS11SigningJob(int numberOfInterval, PKCS11Connector pkcs11Connector, long session,
			PKCS11KeySpecification keySpec) {
		super(numberOfInterval, pkcs11Connector, session);
		this.keySpec = keySpec;
	}

	@Override
	public void process() throws SMVNException {
		CK_MECHANISM ck_MECHANISM = new CK_MECHANISM();
		ck_MECHANISM.mechanism = PKCS11Constants.CKM_RSA_PKCS;
		ck_MECHANISM.pParameter = null;
		long[] privateKey;
		privateKey = this.getPkcs11Connector().findRSAPrivateKey(this.getSession(), this.keySpec.getKeyName());
		if (privateKey.length == 0) {
			LOGGER.error("Private key is not found");
			return;
		}
		byte[] data = "0123456789abcqwertyu".getBytes();
		for (int i = 0; i < this.getNumberOfInterval(); i++) {
			this.getPkcs11Connector().sign(this.getSession(), privateKey[0], ck_MECHANISM, data);
		}

	}

	public PKCS11KeySpecification getKeySpec() {
		return keySpec;
	}

}
