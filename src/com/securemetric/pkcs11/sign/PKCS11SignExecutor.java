package com.securemetric.pkcs11.sign;

import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.pkcs11.PKCS11Executor;
import com.securemetric.pkcs11.PKCS11KeySpecification;
import com.securemetric.throwable.SMVNException;

public class PKCS11SignExecutor extends PKCS11Executor {
	private final PKCS11KeySpecification keySpecification;

	public PKCS11SignExecutor(int numberOfThread, int numberOfInterval) throws SMVNException {
		super(numberOfThread, numberOfInterval);
		this.keySpecification = new PKCS11KeySpecification();
	}

	@Override
	public CommonSigningJob getSinginJob(int currentThread) {
		return new PKCS11SigningJob(this.getNumberOfInterval(), getPkcs11Connector(),
				(long) this.getSessions().get(currentThread), this.keySpecification);
	}

}
