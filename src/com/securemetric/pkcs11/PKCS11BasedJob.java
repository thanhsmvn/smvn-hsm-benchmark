package com.securemetric.pkcs11;

import com.securemetric.benchmark.CommonSigningJob;

public abstract class PKCS11BasedJob extends CommonSigningJob {

	private final PKCS11Connector pkcs11Connector;
	private final long session;

	public PKCS11BasedJob(int numberOfInterval, PKCS11Connector pkcs11Connector, long session) {
		super(numberOfInterval);
		this.pkcs11Connector = pkcs11Connector;
		this.session = session;
	}

	public PKCS11Connector getPkcs11Connector() {
		return pkcs11Connector;
	}

	public long getSession() {
		return session;
	}

}
