package com.securemetric.pkcs11.genkey;

import java.util.concurrent.CountDownLatch;

import com.securemetric.pkcs11.PKCS11BasedJob;
import com.securemetric.pkcs11.PKCS11Connector;
import com.securemetric.pkcs11.PKCS11KeySpecification;
import com.securemetric.throwable.SMVNException;

public class PKCS11GenKeyJob extends PKCS11BasedJob {
	private final PKCS11KeySpecification keySpec;

	public PKCS11GenKeyJob(int numberOfInterval, PKCS11Connector pkcs11Connector, long session,
			PKCS11KeySpecification keySpec) {
		super(numberOfInterval, pkcs11Connector, session);
		this.keySpec = keySpec;
	}

	@Override
	public void process() throws SMVNException {
		int numberOfInterval = this.getNumberOfInterval();
		for (int i = 0; i < numberOfInterval; i++) {
			this.getPkcs11Connector().generateRSAKeyPair(this.getSession(), this.keySpec.getKeyName());
		}
	}

}
