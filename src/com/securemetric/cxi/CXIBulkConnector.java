package com.securemetric.cxi;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.securemetric.throwable.SMVNException;

import CryptoServerAPI.CryptoServerException;
import CryptoServerCXI.CryptoServerCXI;

public class CXIBulkConnector {

    private static final Logger LOGGER = LogManager.getLogger(CXIBulkConnector.class);
    private final CryptoServerCXI cxi;
    private final CryptoServerCXI.Key rsaKey;
    private final byte[][] bulkData;
    private final int sizeOfBulk;
    private final String cxiGroup;

    public CXIBulkConnector()
        throws SMVNException {
        Properties properties = new Properties();
        String cxiUser, cxiPassword, cxiHost;
        try {
            properties.load(new FileInputStream("cxi_config.properties"));
            String size_of_bulk = properties.getProperty("size_of_bulk", "16");
            cxiHost = properties.getProperty("cxi_hsm_ip", "127.0.0.1");
            cxiUser = properties.getProperty("cxi_user", "");
            cxiPassword = properties.getProperty("cxi_password", "");
            this.cxiGroup = properties.getProperty("cxi_group", "");

            this.sizeOfBulk = Integer.parseInt(size_of_bulk);
            this.bulkData = new byte[sizeOfBulk][3];
            for (int i = 0; i < sizeOfBulk; i++) {
                this.bulkData[i] = new byte[] {1, 2, 3};
            }
        } catch (Exception e) {
            LOGGER.error("Cannot construct CXILib", e);
            throw new SMVNException(e);
        }

        try {
            cxi = new CryptoServerCXI(cxiHost, 3000);
            cxi.setTimeout(60000);
            // logon
            cxi.logonPassword(cxiUser, cxiPassword);
            CryptoServerCXI.KeyAttributes attr = new CryptoServerCXI.KeyAttributes();
            attr.setAlgo(CryptoServerCXI.KEY_ALGO_RSA);
            attr.setSize(2048);
            attr.setName("RSA demo");
            attr.setGroup(this.cxiGroup);
            this.rsaKey = cxi.generateKey(CryptoServerCXI.FLAG_OVERWRITE, attr);

        } catch (NumberFormatException e) {
            throw new SMVNException(e);
        } catch (IOException e) {
            throw new SMVNException(e);
        } catch (CryptoServerException e) {
            throw new SMVNException(e);
        }
    }

    public void bulkSign() throws SMVNException {
        try {
            this.cxi
                .bulkSign(this.rsaKey, CryptoServerCXI.MECH_PAD_NONE | CryptoServerCXI.MECH_PAD_PKCS1, this.bulkData);
        } catch (IOException e) {
            throw new SMVNException(e);
        } catch (CryptoServerException e) {
            throw new SMVNException(e);
        }
    }

    public int getSizeOfBulk() {
        return sizeOfBulk;
    }

    public void deleteKey() throws SMVNException {
        try {
            cxi.deleteKey(this.rsaKey);
        } catch (IOException e) {
            throw new SMVNException(e);
        } catch (CryptoServerException e) {
            throw new SMVNException(e);
        }
    }
}
