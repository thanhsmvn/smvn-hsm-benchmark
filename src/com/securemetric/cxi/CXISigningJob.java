package com.securemetric.cxi;

import java.util.concurrent.CountDownLatch;

import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.throwable.SMVNException;

public class CXISigningJob extends CommonSigningJob {

	private final CXIBulkConnector cxiProvider;
	public CXISigningJob(int numberOfInterval, CXIBulkConnector cxiProvider) {
		super(numberOfInterval);
		this.cxiProvider = cxiProvider;
	}

	@Override
	public void process() throws SMVNException {
		for (int i = 0; i < this.getNumberOfInterval(); i++) {
			this.cxiProvider.bulkSign();
		}
	}

}
