package com.securemetric.cxi;

import java.util.concurrent.CountDownLatch;

import com.securemetric.benchmark.BenchmarkExecutor;
import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.throwable.SMVNException;

public class CXIBulkExecutor extends BenchmarkExecutor {

	private final CXIBulkConnector cxiProvider;
    public CXIBulkExecutor(int numberOfThread, int numberOfInterval) throws SMVNException {
		super(numberOfThread, numberOfInterval);
		this.cxiProvider = new CXIBulkConnector();
	}


    @Override
    public void preExecute() {
        LOGGER.info("Size of bulk: " + this.cxiProvider.getSizeOfBulk());
        super.preExecute();
    }

    @Override
    public Double computeTPS() {
        Double tps = super.computeTPS();
        return tps * this.cxiProvider.getSizeOfBulk();
    }

    @Override
    public void postExecute() {
        super.postExecute();
        try {
            this.cxiProvider.deleteKey();
        } catch (SMVNException e) {
            LOGGER.warn("delete cxi key failed", e);
        }
    }

	@Override
	public CommonSigningJob getSinginJob(int currentThread) {
		// TODO Auto-generated method stub
		return null;
	}
    
    

}
