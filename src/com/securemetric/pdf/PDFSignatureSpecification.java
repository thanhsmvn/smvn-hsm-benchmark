package com.securemetric.pdf;


public class PDFSignatureSpecification {
    private String digestAl;
    private byte[] inputFile;
    private String outputFolder;
    private String reason;
    private String location;
    
    public PDFSignatureSpecification(
        String digestAl, byte[] inputFile, String outputFolder, String reason, String location) {
        this.digestAl = digestAl;
        this.inputFile = inputFile;
        this.outputFolder = outputFolder;
        this.reason = reason;
        this.location = location;
    }

    public String getDigestAl() {
        return digestAl;
    }
    
    public void setDigestAl(String digestAl) {
        this.digestAl = digestAl;
    }
    
    public byte[] getInputFile() {
        return inputFile;
    }
    
    public void setInputFile(byte[] inputFile) {
        this.inputFile = inputFile;
    }
    
    public String getOutputFolder() {
        return outputFolder;
    }
    
    public void setOutputFolder(String outputFolder) {
        this.outputFolder = outputFolder;
    }
    
    public String getReason() {
        return reason;
    }
    
    public void setReason(String reason) {
        this.reason = reason;
    }
    
    public String getLocation() {
        return location;
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    
}
