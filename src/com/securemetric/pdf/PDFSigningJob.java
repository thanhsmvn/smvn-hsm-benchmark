package com.securemetric.pdf;

import java.security.PrivateKey;
import java.security.cert.Certificate;

import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.throwable.SMVNException;

public class PDFSigningJob extends CommonSigningJob {

	private final PDFSpecification spec;

	public PDFSigningJob(int numberOfInterval, PDFSpecification spec) {
		super(numberOfInterval);
		this.spec = spec;
	}

	@Override
	public void process() throws SMVNException {
		int numberOfInterval = this.getNumberOfInterval();
		PrivateKey privateKey = this.spec.getPrivateKey();
		Certificate[] certChain = this.spec.getCertChain();
		String providerName = this.spec.getProviderName();
		DocumentSigning docSigning = new DocumentSigning(privateKey, certChain, providerName);
		PDFSignatureSpecification specification = new PDFSignatureSpecification("SHA-256", this.spec.getData(),
				"pdf_output", "I approved", "Ha Noi");
		for (int i = 0; i < numberOfInterval; i++) {
			docSigning.doPDFSigning(specification);
		}
	}

}
