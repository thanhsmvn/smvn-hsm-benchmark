package com.securemetric.pdf;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfAnnotation;
import com.itextpdf.text.pdf.PdfName;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSignatureAppearance;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.security.BouncyCastleDigest;
import com.itextpdf.text.pdf.security.ExternalDigest;
import com.itextpdf.text.pdf.security.ExternalSignature;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import com.itextpdf.text.pdf.security.PrivateKeySignature;
import com.securemetric.throwable.SMVNException;

public class DocumentSigning {

    Logger LOGGER = LogManager.getLogger(DocumentSigning.class);
    private final PrivateKey  privateKey;
    private final Certificate[] certChain; 
    private final String providerName;
    
    public DocumentSigning(PrivateKey privateKey, Certificate[] certChain, String providerName) {
		this.privateKey = privateKey;
		this.certChain = certChain;
		this.providerName = providerName;
	}

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSSS");

    public String doPDFSigning(
         PDFSignatureSpecification specification)
        throws SMVNException {
        // LOGGER.debug("*****************************************");
        // long start = new Date().getTime();
        // LOGGER.debug("Start signing PDF at " + start);
        // LOGGER.debug("*****************************************");

        String fileName = "testpdf";
        try {

            File outputFile = new File(
                specification.getOutputFolder() + "\\" + fileName + "_" + sdf.format(new Date()) + ".pdf");
            FileOutputStream os = new FileOutputStream(outputFile);
            PdfReader reader = new PdfReader(specification.getInputFile());
            PdfStamper stamper = PdfStamper.createSignature(reader, os, '\0');
            // Creating the appearance
            PdfSignatureAppearance appearance = stamper.getSignatureAppearance();
            appearance.setReason(specification.getReason());
            appearance.setLocation(specification.getLocation());
            appearance.setVisibleSignature(new Rectangle(400, 20, 600, 60), 1, null);
            // Creating the signature
            ExternalSignature pks = new PrivateKeySignature(
                privateKey, specification.getDigestAl(), "SunPKCS11-" + providerName);
            ExternalDigest digest = new BouncyCastleDigest();
            MakeSignature.signDetached(appearance, digest, pks, certChain, null, null, null, 0, CryptoStandard.CMS);
            return outputFile.getAbsolutePath();
        } catch (Exception e) {
            throw new SMVNException(e);
        } finally {
            // LOGGER.debug("*****************************************");
            // long end = new Date().getTime();
            // LOGGER.debug("End signing PDF at " + end);
            // LOGGER.debug("*****************************************");
            // LOGGER.debug("*****************************************");
            // LOGGER.debug("Signing PDF process takes " + (end-start) + " miliseconds");
            // LOGGER.debug("*****************************************");
        }
    }

    public void manipulatePdf(String src, String dest) throws SMVNException {
        PdfReader reader;
        try {
            reader = new PdfReader(src);
            PdfStamper watermarkStamper = new PdfStamper(reader, new FileOutputStream(dest), (char) 0, true);
            Image image = Image.getInstance("E:\\Working\\ndthanh\\smvn-coding\\stamp.png");
            image.setAbsolutePosition(0, 0);
            PdfTemplate template = PdfTemplate
                .createTemplate(watermarkStamper.getWriter(), image.getWidth(), image.getHeight());
            template.addImage(image);

            Rectangle rect = new Rectangle(200, 470, 200 + image.getWidth(), 470 + image.getHeight());
            PdfAnnotation annotation = PdfAnnotation
                .createStamp(watermarkStamper.getWriter(), rect, null, "AnnotationOnly");

            annotation.setAppearance(PdfName.N, template);

            watermarkStamper.addAnnotation(annotation, 1);

            watermarkStamper.close();
        } catch (IOException e) {
            LOGGER.error(e);
            throw new SMVNException(e);
        } catch (DocumentException e) {
            LOGGER.error(e);
            throw new SMVNException(e);
        }

    }
}
