package com.securemetric.pdf;

import com.securemetric.jce.JCESpecification;
import com.securemetric.throwable.SMVNException;

public class PDFSpecification extends JCESpecification {

	public PDFSpecification(byte[] data) throws SMVNException {
		super(data);
	}

}
