package com.securemetric.pdf;

import com.securemetric.benchmark.BenchmarkExecutor;
import com.securemetric.benchmark.CommonSigningJob;
import com.securemetric.throwable.SMVNException;

public class PDFExecutor extends BenchmarkExecutor {

	private final PDFSpecification spec;

	public PDFExecutor(int numberOfThread, int numberOfInterval, byte[] data) throws SMVNException {
		super(numberOfThread, numberOfInterval);
		this.spec = new PDFSpecification(data);
	}

	@Override
	public CommonSigningJob getSinginJob(int currentThread) {
		return new PDFSigningJob(this.getNumberOfInterval(), spec);
	}

}
