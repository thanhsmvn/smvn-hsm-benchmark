package com.securemetric.benchmark;

public interface IExecutor {
	 public void execute() ;
	 public void postExecute();
	 public Double computeTPS();
	 public void preExecute();
}
