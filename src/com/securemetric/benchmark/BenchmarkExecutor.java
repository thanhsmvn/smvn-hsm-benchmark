package com.securemetric.benchmark;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public abstract class BenchmarkExecutor implements IExecutor {

	protected final Logger LOGGER = LogManager.getLogger(this.getClass());
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss:SSSS");

	private double executionTime;
	private double tps;
	private long start;
	private long end;
	private final int numberOfThread;
	private final int numberOfInterval;

	public BenchmarkExecutor(int numberOfThread, int numberOfInterval) {
		this.numberOfThread = numberOfThread;
		this.numberOfInterval = numberOfInterval;
	}

	public void execute() {
		this.preExecute();
		ExecutorService executorService = Executors.newFixedThreadPool(numberOfThread);

		this.start = System.currentTimeMillis();
		List<CompletableFuture> futures = new ArrayList<CompletableFuture>();
		try {
			for (int i = 0; i < numberOfThread; i++) {
				futures.add(CompletableFuture.supplyAsync(getSinginJob(i), executorService));
			}
			CompletableFuture.allOf(futures.toArray(new CompletableFuture[futures.size()])).get();
		} catch (Exception e) {
			LOGGER.error(e);
		} finally {
			executorService.shutdown();
		}
		this.end = System.currentTimeMillis();
		executionTime = end - start;
		tps = this.computeTPS();
		this.postExecute();
	}

	public void postExecute() {
		LOGGER.info("End time: " + sdf.format(this.end));
		LOGGER.info("Duration: " + this.executionTime);
		LOGGER.debug("TPS = " + this.tps);
	}

	public Double computeTPS() {
		return Double.valueOf(this.numberOfThread * this.numberOfInterval * 1000 / this.executionTime);
	}

	public void preExecute() {
		LOGGER.info("Number of thread: " + this.numberOfThread);
		LOGGER.info("Number of interval: " + this.numberOfInterval);
		LOGGER.info("Start time: " + sdf.format(System.currentTimeMillis()));
	}

	public abstract CommonSigningJob getSinginJob(int currentThread);

	public int getNumberOfThread() {
		return numberOfThread;
	}

	public int getNumberOfInterval() {
		return numberOfInterval;
	}

}
