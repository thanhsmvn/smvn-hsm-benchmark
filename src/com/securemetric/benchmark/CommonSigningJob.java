package com.securemetric.benchmark;

import java.util.function.Supplier;

import com.securemetric.throwable.SMVNException;

public abstract class CommonSigningJob<T> implements Supplier {

	private final int numberOfInterval;

	public CommonSigningJob(int numberOfInterval) {
		this.numberOfInterval = numberOfInterval;
	}

	public final T get() {
		try {
			this.process();
		} catch (SMVNException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int getNumberOfInterval() {
		return numberOfInterval;
	}

	protected abstract void process() throws SMVNException;

}
