package com.securemetric.benchmark;

public enum SignatureType {
    XML, PDF, BINARY, PKCS11, BULK,P11KEYGEN,LISTOBJ,IMPORTCERT;
    
    public static SignatureType parse(String name) {
        try {
            return SignatureType.valueOf(name.toUpperCase());
        } catch (Exception ex) {
            return BINARY;
        }
    }
}
