package com.securemetric.benchmark;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.securemetric.binary.BinaryExecutor;
import com.securemetric.cxi.CXIBulkExecutor;
import com.securemetric.pdf.PDFExecutor;
import com.securemetric.pkcs11.certimport.PKCS11CertImportExecutor;
import com.securemetric.pkcs11.genkey.PKCS11GenKeyExecutor;
import com.securemetric.pkcs11.listobj.PKCS11ListObjExecutor;
import com.securemetric.pkcs11.sign.PKCS11SignExecutor;
import com.securemetric.throwable.SMVNException;
import com.securemetric.xml.XMLExecutor;

public class ExecutorFactory {
	private static ExecutorFactory INSTANCE;

	private ExecutorFactory() {

	}

	public static ExecutorFactory getInstance() {
		if (INSTANCE == null) {
			INSTANCE = new ExecutorFactory();
		}
		return INSTANCE;
	}

	public BenchmarkExecutor getExecutor() throws SMVNException {
		Properties properties = new Properties();
		try {
			properties.load(new FileInputStream("benchmark.properties"));
			String number_of_thread = properties.getProperty("number_of_thread", "1");
			String number_of_interval = properties.getProperty("number_of_interval", "1");
			String signatureType = properties.getProperty("sign_type", "");
			int numberOfThread = Integer.parseInt(number_of_thread);
			int numberOfInterval = Integer.parseInt(number_of_interval);
			switch (SignatureType.parse(signatureType)) {
			case XML:
				return new XMLExecutor(numberOfThread, numberOfInterval, getByteArrayFromFile("note1.xml"));
			case PDF:
				return new PDFExecutor(numberOfThread, numberOfInterval, getByteArrayFromFile("test.pdf"));
			case BINARY:
				return new BinaryExecutor(numberOfThread, numberOfInterval);
			case PKCS11:
				return new PKCS11SignExecutor(numberOfThread, numberOfInterval);
			case BULK:
				return new CXIBulkExecutor(numberOfThread, numberOfInterval);
			case P11KEYGEN:
				return new PKCS11GenKeyExecutor(numberOfThread, numberOfInterval);
			case LISTOBJ:
				return new PKCS11ListObjExecutor(numberOfThread, numberOfInterval);
			case IMPORTCERT:
				return new PKCS11CertImportExecutor(numberOfThread, numberOfInterval);
			default:
				return null;
			}
		} catch (IOException e) {
			throw new SMVNException(e);
		}

	}

	private byte[] getByteArrayFromFile(String fileName) throws SMVNException {
		FileInputStream fin = null;
		byte[] fileData;
		File file = new File(fileName);
		try {
			fin = new FileInputStream(file);
			fileData = new byte[(int) file.length()];
			fin.read(fileData);

		} catch (FileNotFoundException e) {
			throw new SMVNException(e);
		} catch (IOException e) {
			throw new SMVNException(e);
		} finally {
			if (fin != null) {
				try {
					fin.close();
				} catch (IOException e) {
				}
			}
		}

		return fileData;
	}
}
