package com.securemetric.throwable;


public class SMVNException extends Throwable {
    private static final long serialVersionUID = 8160338232636520809L;

    public SMVNException()
    {
    }

    public SMVNException(String errorMessage)
    {
        super(errorMessage);
    }

    public SMVNException(Throwable ex)
    {
    	super(ex);
    }

}
