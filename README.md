# VCB Sign Service


### Configuration

1. Open `XMLSignatureService.java` and modify the following line
```
// MARK: - URL Config
private static final String PKCS11_PROVIDER_CONFIG = "your_dll_location"
private static final String VCB_CERTIFICATE = "the_label_and_id_of_your_certificate"
private static final String SLOT_ID = "your_slot_id"
private static final String SLOT_PIN = "your_slot_pin"
```

